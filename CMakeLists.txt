cmake_minimum_required(VERSION 3.16.0)

project(fancontrold)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")

set(QT_MIN_VERSION "5.14.0")
set(KF5_MIN_VERSION "5.88")


#options
option(NO_SYSTEMD "Compile without Systemd support. Reduces functionality significantly!" OFF)
option(BUILD_GUI "Build the standalone application" ON)
option(BUILD_KCM "Build the KCM" OFF)
option(BUILD_PLASMOID "Build the plasmoid" OFF)
option(INSTALL_SHARED "Install the shared parts" ON)

#variables
set(STANDARD_SERVICE_NAME "fancontrold" CACHE STRING "The name of the systemd service for the fancontrold daemon")
set(DAEMON_DBUS_INTERFACE_NAME "org.kde.fancontrold" CACHE STRING "The standard dbus interface name to communicate with fancontrold")

add_definitions(-DSTANDARD_SERVICE_NAME="${STANDARD_SERVICE_NAME}")
add_definitions(-DDAEMON_DBUS_INTERFACE_NAME="${DAEMON_DBUS_INTERFACE_NAME}")

#KCM can't be build without systemd support
if(BUILD_KCM AND NO_SYSTEMD)

    message(WARNING "KCM can't be build without systemd support")
    set(BUILD_KCM FALSE)

endif(BUILD_KCM AND NO_SYSTEMD)


#Silence warnings
cmake_policy(SET CMP0063 NEW)


#Find ECM
find_package(ECM 5.38 REQUIRED)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${ECM_MODULE_PATH} ${ECM_KDE_MODULE_DIR})


#includes
include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDECompilerSettings NO_POLICY_SCOPE)
include(FeatureSummary)
include(FindPkgConfig)
include(ECMQMLModules)

include_directories (${CMAKE_SOURCE_DIR})


#Systemd
if(NOT NO_SYSTEMD)

    message(STATUS "Compiling for Systemd")

else(NOT NO_SYSTEMD)

    message(STATUS "Compiling without Systemd")
    set(NO_SYSTEMD true)
    add_definitions(-DNO_SYSTEMD)

endif(NOT NO_SYSTEMD)


#Build the standalone application
if(BUILD_GUI)

    message(STATUS "Build the standalone application")
    add_subdirectory(gui)

endif(BUILD_GUI)


#Build the KCM
if(BUILD_KCM)

    message(STATUS "Build the KCM")
    add_subdirectory(kcm)

endif(BUILD_KCM)


#Build the plasmoid
if(BUILD_PLASMOID)

    message(STATUS "Build the plasmoid")
    add_subdirectory(plasmoid)

endif(BUILD_PLASMOID)


#build and install the shared parts
if(INSTALL_SHARED)

    #qml plugin
    add_subdirectory(import)


    #icons
    include(ECMInstallIcons)
    ecm_install_icons(ICONS sc-apps-org.kde.fancontrold.gui.svg sc-apps-org.kde.fancontrold.gui-light.svg DESTINATION "${KDE_INSTALL_ICONDIR}")


    #translations
    find_package(KF5 COMPONENTS I18n REQUIRED)
    ki18n_install(po)

endif(INSTALL_SHARED)

#summary
feature_summary(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)
