# fancontrold-gui
GUI for Fancontrold.
Furthermore it communicates with systemd via dbus to control the fancontrold service.

If you want to compile without systemd support set the option -DNO_SYSTEMD=true.

If your distro looks for QML plugins in /usr/lib/qt/qml instead of /usr/lib/qml you need to set the option -DKDE_INSTALL_USE_QT_SYS_PATHS=true.

To compile the additional KCM set the cmake option -DBUILD_KCM=on.
The KCM is only build, if the -DNO_SYSTEMD option is unset or set to false.

To compile the additional KDE Plasma plasmoid set the cmake option -DBUILD_PLASMOID=on.

# Build requirements
* Qt5: Base/Core, Widgets, Gui, QML
* KF5: I18n, Config, Package, CoreAddons, DBusAddons, Extra-Cmake-Modules, Notifications
* Other: C++ compiler, Gettext, CMake

# Additional runtime requirements
* Qt5: Quick 2.15, QuickControls2 2.1, QuickLayouts 1.2, QuickDialogs 1.2
* KF5: Kirigami2 2.19

# Additional requirements for KCM
* KF5: KCMUtils, Declarative

# Additional requirements for plasmoid
* KF5: Plasma

# Commands to install requirements
## Debian/Ubuntu command to install the build requirements:
`sudo apt-get install ca-certificates git build-essential cmake gcc g++ libkf5config-dev libkf5package-dev libkf5declarative-dev libkf5coreaddons-dev libkf5dbusaddons-dev libkf5kcmutils-dev libkf5i18n-dev libkf5plasma-dev libqt5core5a libqt5widgets5 libqt5gui5 libqt5qml5 extra-cmake-modules qtbase5-dev libkf5notifications-dev qml-module-org-kde-kirigami2 qml-module-qtquick-dialogs qml-module-qtquick-controls2 qml-module-qtquick-layouts qml-module-qt-labs-settings qml-module-qt-labs-folderlistmodel cmake build-essential gettext`
**Note:** This was tested on `Ubuntu 22.04 LTS` and `Debian 12`.

## Fedora:
```
sudo dnf install git gcc g++ cmake qt5-devel kf5-ki18n-devel kf5-kconfig-devel kf5-kpackage-devel kf5-kcoreaddons-devel kf5-kdbusaddons-devel extra-cmake-modules kf5-knotifications-devel qt5-qtquickcontrols2-devel kf5-kconfigwidgets-devel kf5-kcmutils-devel kf5-plasma-devel cmake gettext qt5-qtbase-devel gcc-c++ kf5-kdeclarative-devel qt5-qtquickcontrols qt5-qtquickcontrols2
```
**Note:** This was tested on `Fedora 33`.

## Example:

```
git clone https://gitlab.com/Maldela/fancontrold-gui.git
cd fancontrol-gui
cmake -B build -DBUILD_KCM=on -DBUILD_PLASMOID=on -DBUILD_TESTING=on -DCMAKE_INSTALL_PREFIX=/usr
cmake --build build -j
sudo cmake --install build
```
