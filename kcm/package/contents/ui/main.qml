/*
 * Copyright (C) 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


import QtQuick 2.6
import QtQuick.Controls 2.9
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.3 as Kirigami
import org.kde.kcm 1.4 as KCM
import Fancontrold.Qml 1.0 as Fancontrold


KCM.AbstractKCM {
    readonly property QtObject loader: Fancontrold.Base.loader
    readonly property QtObject systemdCom: Fancontrold.Base.hasSystemdCommunicator ? Fancontrold.Base.systemdCom : null
    readonly property QtObject pwmFanModel: Fancontrold.Base.pwmFanModel
    readonly property QtObject tempModel: Fancontrold.Base.tempModel
    readonly property QtObject profilesModel: Fancontrold.Base.profilesModel
    readonly property QtObject fan: fansListView.currentItem ? fansListView.currentItem.fan : null

    id: root

    implicitWidth: Kirigami.Units.gridUnit * 50
    implicitHeight: Kirigami.Units.gridUnit * 40

    KCM.ConfigModule.quickHelp: i18n("This module lets you configure fancontrold.")

    Connections {
        target: Fancontrold.Base
        function onNeedsApplyChanged() { kcm.needsSave = Fancontrold.Base.needsApply; }
    }

    Connections {
        target: kcm

        function onAboutToSave() { Fancontrold.Base.apply(); }
        function onAboutToDefault() { loader.disable(); }
    }

    Loader {
        anchors.centerIn: parent
        active: pwmFanModel.length === 0

        sourceComponent: Label {
            text: i18n("There are no pwm capable fans in your system.\nTry running 'sensors-detect' in a terminal and restart this application.")
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 14
            font.bold: true
        }
    }

    ScrollView {
        id: fansListViewBackground

        Component.onCompleted: fansListViewBackground.background.visible = true

        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
        }

        visible: pwmFanModel.length > 0
        width: root.width / 5

        ListView {
            id: fansListView

            clip: true
            boundsBehavior: Flickable.StopAtBounds
            flickableDirection: Flickable.AutoFlickIfNeeded
            model: pwmFanModel
            header: Kirigami.BasicListItem {
                height: toolBarLoader.active ? toolBarLoader.height : implicitHeight
                label: '<b>' + i18n("Fans") + '</b>'
                reserveSpaceForIcon: false
                hoverEnabled: false
                separatorVisible: true
                leftPadding: Kirigami.Units.smallSpacing
            }
            delegate: Kirigami.BasicListItem {
                property QtObject fan: object

                label: name
                subtitle: path
                reserveSpaceForIcon: false
                hoverEnabled: true
                highlighted: ListView.isCurrentItem
                separatorVisible: false

                onPressedChanged: if (pressed) fansListView.currentIndex = index;
            }
        }
    }

    ColumnLayout {
        anchors {
            left: fansListViewBackground.right
            leftMargin: Kirigami.Units.largeSpacing
            right: parent.right
            top: parent.top
            bottom: parent.bottom
        }

        Loader {
            id: toolBarLoader

            active: Fancontrold.Base.hasSystemdCommunicator

            Layout.alignment: Qt.AlignTop
            Layout.fillWidth: true

            sourceComponent: ToolBar {
                RowLayout {
                    anchors.fill: parent

                    Item {
                        Layout.fillWidth: true
                    }

                    ToolButton {
                        Layout.alignment: Qt.AlignRight

                        text: !!systemdCom && systemdCom.serviceActive ? i18n("Stop service") : i18n("Start service")
                        icon.name: !!systemdCom && systemdCom.serviceActive ? "media-playback-stop" : "media-playback-start"

                        onClicked: systemdCom.serviceActive = !systemdCom.serviceActive
                    }

                    ToolButton {
                        Layout.alignment: Qt.AlignRight

                        text: !!systemdCom && systemdCom.serviceEnabled ? i18n("Disable service") : i18n("Enable service")

                        onClicked: systemdCom.serviceEnabled = !systemdCom.serviceEnabled
                    }
                }
            }
        }

        Loader {
            active: !!root.fan

            Layout.fillWidth: true
            Layout.fillHeight: true

            sourceComponent: ColumnLayout {
                Fancontrold.FanHeader {
                    fan: root.fan

                    Layout.alignment: Qt.AlignTop
                    Layout.fillWidth: true
                }

                Fancontrold.FanItem {
                    fan: root.fan

                    Layout.alignment: Qt.AlignBottom
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    onTempDrawerRequested: tempDrawer.open()
                }
            }
        }

        Item {
            id: advancedButton

            property bool expanded: false

            Layout.alignment: Qt.AlignBottom
            Layout.fillWidth: true
            Layout.preferredHeight: childrenRect.height

            Image {
                id: advancedArrow

                source: parent.expanded ? "image://icon/go-down" : "image://icon/go-next"
                fillMode: Image.PreserveAspectFit
                height: advancedLabel.implicitHeight
            }
            Label {
                id: advancedLabel

                anchors.left: advancedArrow.right
                text: i18n("Advanced settings")
                font.bold: true
            }
            MouseArea {
                anchors.fill: parent
                onClicked: parent.expanded = !parent.expanded
            }
        }

        Fancontrold.SettingsForm {
            id: settingsArea

            showAll: false
            clip: true
            state: advancedButton.expanded ? "VISIBLE" : "HIDDEN"

            Layout.alignment: Qt.AlignBottom
            Layout.fillWidth: true

            states: [
                State {
                    name: "VISIBLE"

                    PropertyChanges {
                        target: settingsArea
                        Layout.preferredHeight: implicitHeight
                    }
                },
                State {
                    name: "HIDDEN"

                    PropertyChanges {
                        target: settingsArea
                        Layout.preferredHeight: 0
                    }
                }
            ]

            transitions: Transition {
                NumberAnimation {
                    properties: "Layout.preferredHeight"
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }

    Kirigami.OverlayDrawer {
        id: tempDrawer

        parent: root.contentItem
        edge: Qt.RightEdge
        modal: true
        width: contentItem.implicitWidth + leftPadding + rightPadding
        height: fansListViewBackground.height

        contentItem: Fancontrold.TempList {
            fan: root.fan
        }
    }

    Fancontrold.ErrorDisplay {
        anchors.fill: parent
    }
}
