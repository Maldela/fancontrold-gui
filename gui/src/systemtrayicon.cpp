/*
 * Copyright 2018  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "systemtrayicon.h"

#include <KLocalizedString>


#ifndef CONFIG_NAME
#define CONFIG_NAME "fancontrold-gui"
#endif


SystemTrayIcon::SystemTrayIcon(QObject *parent) : KStatusNotifierItem(QStringLiteral("fancontrold.gui"), parent)
{
    setTitle(i18n("Fancontrold-GUI"));
    setCategory(KStatusNotifierItem::ApplicationStatus);
    setIconByName(QStringLiteral("org.kde.fancontrold.gui"));

    m_profilesMenu = contextMenu()->addMenu(i18n("Apply profile"));
}

void SystemTrayIcon::setProfilesModel(QAbstractListModel* model)
{
    if (m_profilesModel == model)
        return;

    m_profilesModel = model;
    emit profilesModelChanged();

    updateProfiles();

    if (!m_profilesModel)
        return;

    connect(m_profilesModel, &QAbstractListModel::dataChanged, this, &SystemTrayIcon::updateProfiles);
    connect(m_profilesModel, &QAbstractListModel::rowsInserted, this, &SystemTrayIcon::updateProfiles);
    connect(m_profilesModel, &QAbstractListModel::rowsRemoved, this, &SystemTrayIcon::updateProfiles);
    connect(m_profilesModel, &QAbstractListModel::modelReset, this, &SystemTrayIcon::updateProfiles);
}

void SystemTrayIcon::updateProfiles()
{
    m_profilesMenu->clear();

    if (!m_profilesModel)
        return;

    auto rows = m_profilesModel->rowCount();

    for (int row = 0; row < rows; row++)
    {
        auto index = m_profilesModel->index(row, 0);
        auto profile = index.data(0).toString();
        m_profilesMenu->addAction(profile, this, [this, profile] () { emit activateProfile(profile); });
    }
}
