/*
 * Copyright (C) 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


import QtQuick 2.15
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.10 as Kirigami
import Fancontrold.Qml 1.0 as Fancontrold


Kirigami.ScrollablePage {
    id: root

    readonly property var hwmons: Fancontrold.Base.loader.hwmons

    title: i18n("Sensors")

    spacing: Kirigami.Units.smallSpacing

    ListView {
        id: listView

        width: root.width
        topMargin: spacing
        spacing: Kirigami.Units.largeSpacing
        headerPositioning: ListView.OverlayHeader
        reuseItems: true

        model: Fancontrold.Base.sensorModel

        section.property: "hwmon"
        section.delegate: Kirigami.ListSectionHeader {
            label: section
        }

        delegate: Kirigami.BasicListItem {
            label: display
            subtitle: path
        }
    }
}
