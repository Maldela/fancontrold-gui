/*
 * Copyright (C) 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


import QtQuick 2.6
import QtQuick.Controls 2.1
import org.kde.kirigami 2.3 as Kirigami
import Fancontrold.Qml 1.0 as Fancontrold


Kirigami.Page {
    readonly property QtObject loader: Fancontrold.Base.loader
    readonly property QtObject systemdCom: Fancontrold.Base.hasSystemdCommunicator ? Fancontrold.Base.systemdCom : null
    readonly property QtObject pwmFanModel: Fancontrold.Base.pwmFanModel
    property QtObject fan

    id: root

    title: i18n("Fans")

    header: Fancontrold.FanHeader {
        fan: root.fan
    }

    actions.contextualActions: [
        Kirigami.Action {
            text: i18n("Manage profiles")
            onTriggered: {
                var profilesSheet = profilesSheetComponent.createObject(root);

                if (profilesSheet == null)
                    console.log("Error creating profiles sheet");
                else
                    profilesSheet.open();
            }
        },
        Kirigami.Action {
            text: i18n("Service")
            visible: Fancontrold.Base.hasSystemdCommunicator
            tooltip: i18n("Control the systemd service")

            Kirigami.Action {
                text: !!systemdCom && systemdCom.serviceActive ? i18n("Stop service") : i18n("Start service")
                icon.name: !!systemdCom && systemdCom.serviceActive ? "media-playback-stop" : "media-playback-start"

                onTriggered: systemdCom.serviceActive = !systemdCom.serviceActive
            }
            Kirigami.Action {
                text: !!systemdCom && systemdCom.serviceEnabled ? i18n("Disable service") : i18n("Enable service")
                tooltip: !!systemdCom && systemdCom.serviceEnabled ? i18n("Disable service autostart at boot") : i18n("Enable service autostart at boot")

                onTriggered: systemdCom.serviceEnabled = !systemdCom.serviceEnabled
            }
        }
    ]

    actions.main: Kirigami.Action {
        text: i18n("Apply")
        enabled: Fancontrold.Base.needsApply
        icon.name: "dialog-ok-apply"
        tooltip: i18n("Apply your new config.")
        shortcut: StandardKey.Apply

        onTriggered: Fancontrold.Base.apply()
    }
    actions.right: Kirigami.Action {
        text: i18n("Reset")
        enabled: Fancontrold.Base.needsApply
        icon.name: "edit-undo"
        tooltip: i18n("Revert changes")

        onTriggered: Fancontrold.Base.reset()
    }

    Loader {
        asynchronous: true
        visible: status == Loader.Ready
        anchors.fill: parent
        active: !!root.fan

        sourceComponent: Fancontrold.FanItem {
            fan: root.fan

            onTempDrawerRequested: tempDrawer.open()
        }
    }

    Kirigami.OverlayDrawer {
        id: tempDrawer

        edge: Qt.RightEdge
        modal: true
        handleVisible: drawerOpen

        contentItem: Loader {
            active: !!root.fan

            sourceComponent: Fancontrold.TempList {
                fan: root.fan
            }
        }
    }

    Loader {
        anchors.centerIn: parent
        active: pwmFanModel.length === 0

        sourceComponent: Label {
            text: i18n("There are no pwm capable fans in your system.\nTry running 'sensors-detect' in a terminal and restart this application.")
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 14
            font.bold: true
        }
    }

    Component {
        id: profilesSheetComponent

        Kirigami.OverlaySheet {
            title: i18n("Profiles")

            contentItem: Fancontrold.Profiles {}
        }
    }
}
