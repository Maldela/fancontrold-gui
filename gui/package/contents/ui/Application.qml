/*
 * Copyright (C) 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.19 as Kirigami
import Fancontrold.Gui 1.0 as Gui
import Fancontrold.Qml 1.0 as Fancontrold


Kirigami.ApplicationWindow {
    id: window

    function showWindow() {
        window.show()
        window.raise()
        window.requestActivate()
    }

    function lightness(color) {
        return (color.r + color.g + color.b) / 3;
    }

    title: i18n("Fancontrold-GUI")

    minimumWidth: Kirigami.Units.gridUnit * 20
    minimumHeight: Kirigami.Units.gridUnit * 15

    reachableModeEnabled: false

    pageStack.globalToolBar.style: Kirigami.ApplicationHeaderStyle.ToolBar
    pageStack.globalToolBar.showNavigationButtons: Kirigami.ApplicationHeaderStyle.NoNavigationButtons
    pageStack.defaultColumnWidth: width - drawer.width
    pageStack.initialPage: sensorsTab

    Component.onCompleted: {
        window.visible = !(Fancontrold.Base.startMinimized && Fancontrold.Base.showTray);
    }

    onWideScreenChanged: drawer.drawerOpen = wideScreen
    onClosing: if (!Fancontrold.Config.showTray) { Qt.quit(); }

    globalDrawer: Kirigami.GlobalDrawer {
        id: drawer

        modal: !window.wideScreen
        handleVisible: true
        resetMenuOnTriggered: false
        contentItem.implicitWidth: Kirigami.Units.gridUnit * 10

        Component.onCompleted: {
            var actions = [];
            for (var i=0; i<Fancontrold.Base.pwmFanModel.length; i++) {
                var action = fanActionComponent.createObject(fansAction, { "index": i });
                actions.push(action);
            }
            fansAction.children = actions;
        }

        actions: [
            Kirigami.Action {
                text: i18n("Sensors")
                iconName: "computer"
                checked: sensorsTab.visible

                onTriggered: {
                    if (!sensorsTab.visible) {
                        while (pageStack.depth > 0) {
                            pageStack.pop();
                        }
                        pageStack.push(sensorsTab);
                    }
                }
            },
            Kirigami.Action {
                id: fansAction

                text: i18n("Fans")
                iconName: lightness(drawer.background.color) >= 127 ? "org.kde.fancontrold.gui" : "org.kde.fancontrold.gui-light"
                checked: pwmFansTab.visible

                onTriggered: {
                    if (!pwmFansTab.visible) {
                        while (pageStack.depth > 0) {
                            pageStack.pop();
                        }
                        pageStack.push(pwmFansTab);
                    }
                }
            },
            Kirigami.Action {
                text: i18n("Config")
                iconName: "text-x-generic"
                checked: configfileTab.visible

                onTriggered: {
                    if (!configfileTab.visible) {
                        while (pageStack.depth > 0) {
                            pageStack.pop();
                        }
                        pageStack.push(configfileTab);
                    }
                }
            },
            Kirigami.Action {
                text: i18n("Settings")
                iconName: "preferences-other"
                checked: settingsTab.visible

                onTriggered: {
                    if (!settingsTab.visible) {
                        while (pageStack.depth > 0) {
                            pageStack.pop();
                        }
                        pageStack.push(settingsTab);
                    }
                }
            },
            Kirigami.Action {
                text: i18n("About")
                iconName: "help-about"
                checked: aboutPage.visible

                onTriggered: {
                    if (!aboutPage.visible) {
                        while (pageStack.depth > 0) {
                            pageStack.pop();
                        }
                        pageStack.push(aboutPage);
                    }
                }
            }

        ]
    }

    Component {
        id: fanActionComponent

        Kirigami.Action {
            required property int index
            readonly property QtObject fan: Fancontrold.Base.pwmFanModel.fan(index)

            text: !!fan ? fan.name : ""
            visible: !!fan
            checked: pwmFansTab.fan === fan

            onTriggered: pwmFansTab.fan = fan
        }
    }

    SensorsTab {
        id: sensorsTab

        visible: false
    }

    PwmFansTab {
        id: pwmFansTab

        visible: false
        fan: Fancontrold.Base.pwmFanModel.fan(0)
    }

    ConfigfileTab {
        id: configfileTab

        visible: false
    }

    SettingsTab {
        id: settingsTab

        visible: false
    }

    Kirigami.AboutPage {
        id: aboutPage

        visible: false
        aboutData: Gui.About
    }

    Fancontrold.ErrorDisplay {
        id: errorDisplay

        anchors.fill: parent
        anchors.margins: Kirigami.Units.largeSpacing
    }

    contextDrawer: Kirigami.ContextDrawer {}

    Loader {
        id: trayLoader

        active: Fancontrold.Config.showTray

        sourceComponent: Gui.SystemTrayIcon {
            iconName: Fancontrold.Base.darkTray ? "org.kde.fancontrold.gui" : "org.kde.fancontrold.gui-light"

            profilesModel: Fancontrold.Base.profilesModel

            onActivateRequested: window.showWindow()
            onActivateProfile: {
                Fancontrold.Base.applyProfile(profile);
                Fancontrold.Base.apply();
            }
        }
    }
}
