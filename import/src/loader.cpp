/*
 * Copyright (C) 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


#include "loader.h"

#include "guibase.h"
#include "hwmon.h"
#include "sensors/pwmfan.h"
#include "sensors/fan.h"
#include "sensors/virtualtemp.h"
#include "interface.h"

#include <QtCore/QFile>
#include <QtCore/QDir>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonValue>
#include <QtCore/QTextStream>
#include <QtCore/QTimer>

#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusConnectionInterface>
#include <QtDBus/QDBusPendingCallWatcher>
#include <QtDBus/QDBusReply>

#include <KI18n/KLocalizedString>


#define HWMON_PATH "/sys/class/hwmon"
#define DBUS_CONNECTION "org.kde.fancontrold"
#define DBUS_PATH "/org/kde/fancontrold"


namespace Fancontrold
{

class HwmonDisconnector
{
public:

    HwmonDisconnector(Loader *loader) : m_loader(loader)
    {
        const auto hwmons = m_loader->hwmons();
        for (const auto &hwmon : hwmons)
            QObject::disconnect(hwmon, &Hwmon::configUpdateNeeded, loader, &Loader::updateConfig);
    }

    ~HwmonDisconnector()
    {
        const auto hwmons = m_loader->hwmons();
        for (const auto &hwmon : hwmons)
            QObject::connect(hwmon, &Hwmon::configUpdateNeeded, m_loader, &Loader::updateConfig, Qt::QueuedConnection);
    }

private:
    Q_DISABLE_COPY(HwmonDisconnector)
    Loader *m_loader;
};


Loader::Loader(GUIBase *parent, bool parseHwmons) : QObject(parent),
    m_timer(new QTimer(this)),
    m_daemonConnected(false),
    m_dbusInterface(nullptr)
{
    connect(this, &Loader::configChanged, this, &Loader::needsApplyChanged);
    connect(this, &Loader::daemonConfigChanged, this, &Loader::needsApplyChanged);
    connect(this, &Loader::daemonConnectedChanged, this, &Loader::needsApplyChanged);

    if (parent)
    {
        connect(this, &Loader::error, parent, &GUIBase::handleError);
        connect(this, &Loader::info, parent, &GUIBase::handleInfo);
    }

    m_timer->setSingleShot(false);
    m_timer->start(1000);

    connect(m_timer, &QTimer::timeout, this, &Loader::sensorsUpdateNeeded);
    connect(m_timer, &QTimer::timeout, this, &Loader::updateDaemonConnected);

    if (parseHwmons)
        this->parseHwmons();

    auto systemInterface = QDBusConnection::systemBus().interface();
    connect(systemInterface, &QDBusConnectionInterface::serviceRegistered, this, &Loader::onServiceRegistered, Qt::QueuedConnection);
    connect(systemInterface, &QDBusConnectionInterface::serviceUnregistered, this, &Loader::onServiceUnregistered, Qt::QueuedConnection);

    connectDBus();
}

void Loader::connectDBus(QString connection, QString path)
{
    if (connection.isEmpty())
        connection = DBUS_CONNECTION;

    if (path.isEmpty())
        path = DBUS_PATH;

    if (m_dbusInterface)
    {
        if (m_dbusInterface->isValid())
        {
            if (m_dbusInterface->service() == connection && m_dbusInterface->path() == path)
                return;
            else
                qDebug() << "DBus connection or path changed";
        }

        m_dbusInterface->deleteLater();
        m_dbusInterface = nullptr;
    }

    m_dbusInterface = new OrgKdeFancontroldInterface(connection, path, QDBusConnection::systemBus(), this);

    if (!m_dbusInterface->isValid())
    {
        emit error(i18n("Could not connect to daemon dbus interface: \'%1\'", m_dbusInterface->lastError().message()));
        m_dbusInterface->deleteLater();
        m_dbusInterface = nullptr;
        return;
    }

    auto config = m_dbusInterface->config();

    onDaemonConfigChanged(config);
    load(config);

    connect(m_dbusInterface, &OrgKdeFancontroldInterface::Error, this, &Loader::onDaemonError);
    connect(m_dbusInterface, &OrgKdeFancontroldInterface::NewConfig, this, &Loader::onDaemonConfigChanged);
    connect(m_dbusInterface, &OrgKdeFancontroldInterface::Starting, this, &Loader::daemonConnectedChanged);
    connect(m_dbusInterface, &OrgKdeFancontroldInterface::Quitting, this, &Loader::daemonConnectedChanged);

    updateDaemonConnected();
}

void Loader::parseHwmons(QString path)
{
    if (path.isEmpty())
        path = QStringLiteral(HWMON_PATH);

    const auto hwmonDir = QDir(path);
    QStringList list;

    if (hwmonDir.isReadable())
    {
        list = hwmonDir.entryList(QDir::AllEntries | QDir::NoDotAndDotDot);
    }
    else if (hwmonDir.exists())
    {
        emit error(i18n("Hwmon path is not readable: \'%1\'", path));
        return;
    }
    else
    {
        emit error(i18n("Hwmon path does not exist: \'%1\'", path));
        return;
    }

    QStringList dereferencedList;
    while (!list.isEmpty())
        dereferencedList << QFile::symLinkTarget(hwmonDir.absoluteFilePath(list.takeFirst()));

    for (auto &hwmon : std::as_const(m_hwmons))
    {
        hwmon->deleteLater();
    }
    m_hwmons.clear();

    for (const auto &hwmonPath : qAsConst(dereferencedList))
    {
        auto newHwmon = new Hwmon(hwmonPath, this);

        if (m_hwmons.contains(newHwmon->index()))
        {
            emit warning(i18n("An Hwmon with index %1 already exists.", newHwmon->index()));
            continue;
        }

        if (newHwmon->isValid())
        {
            connect(this, &Loader::sensorsUpdateNeeded, newHwmon, &Hwmon::sensorsUpdateNeeded, Qt::QueuedConnection);
            m_hwmons.insert(newHwmon->index(), newHwmon);
            emit hwmonsChanged();
        }
        else
        {
            delete newHwmon;
            emit warning(i18n("Invalid hwmon found at: %1", hwmonPath));
        }
    }
}

PwmFan * Loader::pwmFan(uint hwmonIndex, uint pwmFanIndex) const
{
    const auto hwmon = m_hwmons.value(hwmonIndex, Q_NULLPTR);

    if (!hwmon)
        return Q_NULLPTR;

    return hwmon->pwmFans().value(pwmFanIndex);
}

Temp * Loader::temp(uint hwmonIndex, uint tempIndex) const
{
    const auto hwmon = m_hwmons.value(hwmonIndex, Q_NULLPTR);

    if (!hwmon)
        return Q_NULLPTR;

    return hwmon->temps().value(tempIndex);
}

Fan * Loader::fan(uint hwmonIndex, uint fanIndex) const
{
    const auto hwmon = m_hwmons.value(hwmonIndex, Q_NULLPTR);

    if (!hwmon)
        return Q_NULLPTR;

    return hwmon->fans().value(fanIndex);
}

Fancontrold::Hwmon * Loader::hwmon(const QString& hwmon_device_path) const
{
    for (const auto &hwmon : std::as_const(m_hwmons))
    {
        if (hwmon->device_path() == hwmon_device_path)
            return hwmon;
    }
    return Q_NULLPTR;
}

bool Loader::parseConfig(const QJsonDocument &config)
{
    if (m_config == config)
        return true;

    HwmonDisconnector dis(this);

    toDefault();

    if (config.isEmpty())
        return true;

    if (!config.isObject())
    {
        emit error(i18n("Config is not a json object."));
        return false;
    }

    auto c = config.object();

    auto channels = c.value(QStringLiteral("channels"));
    if (!channels.isArray())
    {
        emit error(i18n("Fans config entry is not an array."));
        return false;
    }

    const auto channelsArray = channels.toArray();

    for (const auto &channelConfig : channelsArray)
    {
        if (!channelConfig.isObject())
        {
            emit warning(i18n("Config for channel is not an object."));
            continue;
        }

        auto channelObject = channelConfig.toObject();

        auto hwmonDevicePathValue = channelObject.value(QStringLiteral("hwmon_device_path"));
        if (!hwmonDevicePathValue.isString())
        {
            emit warning(i18n("Config for channel has no valid hwmon device path."));
            continue;
        }
        auto hwmonDevicePath = hwmonDevicePathValue.toString();

        auto hwmon = this->hwmon(hwmonDevicePath);
        if (!hwmon)
        {
            emit warning(i18n("No hwmon with device path %1", hwmonDevicePath));
            continue;
        }

        auto indexValue = channelObject.value(QStringLiteral("index"));
        if (!indexValue.isDouble())
        {
            emit warning(i18n("Config for channel has no valid index."));
            continue;
        }
        int index = indexValue.toDouble();

        auto pwm = hwmon->pwmFans().value(index);
        if (!pwm)
        {
            emit warning(i18n("Fan has invalid index."));
            continue;
        }

        auto tempsValue = channelObject.value(QStringLiteral("temps"));
        if (!tempsValue.isArray())
            emit warning(i18n("Config for channel temps is not an array."));
        else
        {
            const auto tempsArray = tempsValue.toArray();

            QList<Temp*> temps;
            for (const auto &tempConfig : tempsArray)
            {
                if (!tempConfig.isObject())
                {
                    emit warning(i18n("Config for temp is not an object."));
                    continue;
                }

                auto tempObject = tempConfig.toObject();

                auto hwmonDevicePathValue = tempObject.value(QStringLiteral("hwmon_device_path"));
                if (!hwmonDevicePathValue.isString())
                {
                    emit warning(i18n("Config for channel has no valid hwmon device path."));
                    continue;
                }
                auto hwmonDevicePath = hwmonDevicePathValue.toString();

                auto hwmon = this->hwmon(hwmonDevicePath);
                if (!hwmon)
                {
                    emit warning(i18n("No hwmon with device path %1", hwmonDevicePath));
                    continue;
                }

                auto indexValue = tempObject.value(QStringLiteral("index"));
                if (!indexValue.isDouble())
                {
                    emit warning(i18n("Config for temp has no valid index."));
                    continue;
                }
                int index = indexValue.toDouble();

                auto temp = hwmon->temps().value(index);
                if (!temp)
                {
                    emit warning(i18n("Temp has invalid index."));
                    continue;
                }

                temps << temp;
            }

            pwm->setTemps(temps);
        }

        auto virtualTempsValue = channelObject.value(QStringLiteral("virtual_temps"));
        if (!virtualTempsValue.isArray())
            emit warning(i18n("Config for channel virtual temps is not an array."));
        else
        {
            const auto virtualTempsArray = virtualTempsValue.toArray();

            QList<VirtualTemp*> virtualTemps;
            for (const auto &virtualTempConfig : virtualTempsArray)
            {
                if (!virtualTempConfig.isString())
                {
                    emit warning(i18n("Config for virtual temp is not a string."));
                    continue;
                }

                auto virtualTempPath = virtualTempConfig.toString();

                if (virtualTempPath.isEmpty())
                {
                    emit warning(i18n("Config for virtual temp has empty path."));
                    continue;
                }

                auto virtualTemp = this->virtualTemp(virtualTempPath);
                if (!virtualTemp)
                {
                    emit warning("No virtual temp with path " + virtualTempPath);
                    continue;
                }

                virtualTemps << virtualTemp;
            }

            pwm->setVirtualTemps(virtualTemps);

            if (virtualTemps.isEmpty() && pwm->temps().isEmpty())
                emit warning(i18n("Config for channel has no valid temps."));
        }

        auto controlValue = channelObject.value(QStringLiteral("control"));
        if (!controlValue.isObject())
            emit warning(i18n("Config for channel control is not an object"));
        else
        {
            auto controlObject = controlValue.toObject();
            auto curveValue = controlObject.value(QStringLiteral("Curve"));
            auto anchorValue = controlObject.value(QStringLiteral("Anchor"));

            if (curveValue.isObject())
            {
                auto curveObject = curveValue.toObject();
                const auto keys = curveObject.keys();
                Curve curve;
                for (const auto &key : keys)
                {
                    int x = key.toInt();
                    int y = curveObject.value(key).toDouble();
                    curve << QPoint(x, y);
                }
                pwm->setCurve(curve);
                pwm->setControlMode(PwmFan::CurveControl);
            }
            else if (anchorValue.isDouble())
            {
                auto anchor = anchorValue.toDouble();
                pwm->setAnchor(anchor);
                pwm->setControlMode(PwmFan::AnchorControl);
            }
            else
                emit warning(i18n("No valid curve or anchor values in control config"));
        }

        auto minStartValue = channelObject.value(QStringLiteral("min_start"));
        if (!minStartValue.isDouble())
            emit warning(i18n("Min_start is not a number"));
        else
            pwm->setMinStart(minStartValue.toDouble());

        auto minPwmValue = channelObject.value(QStringLiteral("min_pwm"));
        if (!minPwmValue.isDouble())
            emit warning(i18n("Min_pwm is not a number"));
        else
            pwm->setMinPwm(minPwmValue.toDouble());

        auto mtfValue = channelObject.value(QStringLiteral("mtf"));
        if (!mtfValue.isString())
            emit warning(i18n("Mtf is not a string"));
        else
            pwm->setMtf(mtfValue.toString());

        auto pwmModeValue = channelObject.value(QStringLiteral("pwm_mode"));
        if (!pwmModeValue.isString())
            emit warning(i18n("Pwm mode is not a string"));
        else
            pwm->setPwmMode(pwmModeValue.toString());

        auto averageValue = channelObject.value(QStringLiteral("average"));
        if (!averageValue.isDouble())
            emit warning(i18n("Average is not a number"));
        else
            pwm->setAverage(averageValue.toDouble());
    }

    updateConfig();

    return true;
}

bool Loader::load(const QString &newConfig)
{
    if (config() == newConfig)
        return true;

    QJsonParseError err;
    auto newJson = QJsonDocument::fromJson(newConfig.toUtf8(), &err);

    if (err.error != QJsonParseError::NoError)
    {
        emit error(i18n("Error converting config to json object: \'%1\'", err.errorString()));
        return false;
    }

    return parseConfig(newJson);
}

void Loader::apply()
{
    if (config().isEmpty())
    {
        emit error(i18n("Current config is invalid!"));
        return;
    }

    if (!daemonConnected())
    {
        emit error(i18n("Daemon not connected"));
        return;
    }

    auto checkReply = m_dbusInterface->Check(config());
    auto watcher = new QDBusPendingCallWatcher(checkReply, this);
    connect(watcher, &QDBusPendingCallWatcher::finished, this, [=] () { this->onDaemonCheckReplyReceived(watcher, true); });
}

void Loader::save()
{
    if (!m_dbusInterface || !m_dbusInterface->isValid() || !m_dbusInterface->isConnected())
    {
        emit error(i18n("Not connected to daemon."));
        return;
    }

    QString path = m_dbusInterface->configPath();

    if (path.isEmpty())
        path = QStringLiteral("/etc/fancontrold.json");

    QFile configFile(path);

    if (!configFile.open(QIODevice::WriteOnly))
    {
        emit error(i18n("File %1 is not writable. Are you a member of the \'fancontrold\' group?", path));
        return;
    }

    if (configFile.write(config().toUtf8()) == -1)
    {
        emit error(i18n("Error writing to file %1.", path));
    }
}

void Loader::reset()
{
    if (m_config == m_daemonConfig)
        return;

    parseConfig(m_daemonConfig);
}

void Loader::disable()
{
    parseConfig(QJsonDocument());
}

void Loader::updateConfig()
{
    const auto config = createConfig();

    if (config.toJson() != m_config.toJson())
    {
        m_config = config;
        emit configChanged();
    }
}

void Loader::onServiceRegistered(const QString& serviceName)
{
    if (serviceName != DBUS_CONNECTION)
        return;

    connectDBus();
}

void Loader::onServiceUnregistered(const QString& serviceName)
{
    if (serviceName != DBUS_CONNECTION)
        return;

    m_dbusInterface->deleteLater();
    m_dbusInterface = nullptr;

    emit daemonConnectedChanged();
}

void Loader::updateDaemonConnected()
{
    connectDBus();

    if (!m_dbusInterface)
    {
        m_daemonConnected = false;
        emit daemonConnectedChanged();
        return;
    }

    auto daemonConnected = m_dbusInterface->isConnected();
    if (daemonConnected == m_daemonConnected)
        return;

    m_daemonConnected = daemonConnected;
    emit daemonConnectedChanged();
}

QJsonDocument Loader::createConfig() const
{
    QJsonObject root;

    auto hwmons = m_hwmons.values();
    std::sort(hwmons.begin(), hwmons.end(), [] (Hwmon *a, Hwmon *b) { return a->device_path() < b->device_path(); });

    // Add channels
    QJsonArray channels;
    for (const auto &hwmon : std::as_const(hwmons))
    {
        //Sort PwmFans
        auto pwmFans = hwmon->pwmFans().values();
        std::sort(pwmFans.begin(), pwmFans.end(), [] (PwmFan *a, PwmFan *b) { return a->index() < b->index(); });

        for (const auto &pwm : std::as_const(pwmFans))
        {
            if (pwm->controlMode() == PwmFan::NoControl)
                continue;

            if (pwm->temps().isEmpty())
            {
                emit warning(i18n("Pwm %1 has no temps!", pwm->name()));
                continue;
            }

            QJsonObject fanConfig;

            fanConfig.insert(QStringLiteral("hwmon_device_path"), hwmon->device_path());
            fanConfig.insert(QStringLiteral("index"), int(pwm->index()));

            auto temps = pwm->temps();
            std::sort(temps.begin(), temps.end(), [] (Temp *a, Temp *b) {
                if (a->parent()->device_path() == b->parent()->device_path())
                    return a->index() < b->index();
                else
                    return a->parent()->device_path() < b->parent()->device_path();
            });
            QJsonArray configTemps;
            for (const auto &temp : std::as_const(temps))
            {
                QJsonObject tempConfig;
                tempConfig.insert(QStringLiteral("hwmon_device_path"), temp->parent()->device_path());
                tempConfig.insert(QStringLiteral("index"), (int)temp->index());
                configTemps.append(tempConfig);
            }
            fanConfig.insert(QStringLiteral("temps"), configTemps);

            auto virtualTemps = pwm->virtualTemps();
            std::sort(virtualTemps.begin(), virtualTemps.end(), [] (VirtualTemp *a, VirtualTemp *b) {
                return a->path() < b->path();
            });
            QJsonArray configVirtualTemps;
            for (const auto &virtualTemp : std::as_const(virtualTemps))
            {
                configVirtualTemps.append(QJsonValue(virtualTemp->path()));
            }
            fanConfig.insert(QStringLiteral("virtual_temps"), configVirtualTemps);

            switch (pwm->controlMode())
            {
                case PwmFan::AnchorControl:
                {
                    QJsonObject controlObject;
                    controlObject.insert(QStringLiteral("Anchor"), pwm->anchor());
                    fanConfig.insert(QStringLiteral("control"), controlObject);
                    break;
                }

                case PwmFan::CurveControl:
                {
                    QJsonObject controlObject;
                    QJsonObject curve;
                    const auto pwmCurve = pwm->curve();
                    if (pwmCurve.isEmpty())
                    {
                        emit warning(i18n("Pwm %1 has no curve points!", pwm->name()));
                        continue;
                    }
                    else
                    {
                        for (const auto &point : pwmCurve)
                        {
                            curve.insert(QString::number(point.x()), point.y());
                        }
                    }
                    controlObject.insert(QStringLiteral("Curve"), curve);
                    fanConfig.insert(QStringLiteral("control"), controlObject);
                    break;
                }

                case PwmFan::NoControl:
                {
                    break;
                }
            }

            fanConfig.insert(QStringLiteral("min_start"), pwm->minStart());
            fanConfig.insert(QStringLiteral("average"), pwm->average());
            fanConfig.insert(QStringLiteral("min_pwm"), pwm->minPwm());
            QString mtf = pwm->mtf() == PwmFan::Min ? QStringLiteral("Min") : pwm->mtf() == PwmFan::Max ? QStringLiteral("Max") : QStringLiteral("Average");
            fanConfig.insert(QStringLiteral("mtf"), mtf);
            QString pwmMode = pwm->pwmMode() == PwmFan::DC ? QStringLiteral("DC") : pwm->pwmMode() == PwmFan::PWM ? QStringLiteral("PWM") : QStringLiteral("Automatic");
            fanConfig.insert(QStringLiteral("pwm_mode"), pwmMode);

            channels.append(fanConfig);
        }
    }
    root.insert(QStringLiteral("channels"), channels);

    return QJsonDocument(root);
}

void Loader::onDaemonConfigChanged(const QString& daemonConfig)
{
    QJsonParseError err;
    auto newDaemonConfig = QJsonDocument::fromJson(daemonConfig.toUtf8(), &err);

    if (err.error != QJsonParseError::NoError) {
        emit error(i18n("Error converting daemon config to json object: \'%1\'", err.errorString()));
    }

    if (m_daemonConfig == newDaemonConfig)
        return;

    m_daemonConfig = newDaemonConfig;
    emit daemonConfigChanged();
}

void Loader::onDaemonError(const QString& e)
{
    emit error(i18n("Daemon encountered an error: %1", e));
}

void Loader::onDaemonApplyReplyReceived(QDBusPendingCallWatcher* watcher)
{
    QDBusPendingReply<> reply = *watcher;

    if (reply.isError())
        emit error(i18n("DBus error while applying config to daemon: \'%1\'", reply.error().message()));

    watcher->deleteLater();
}

void Loader::onDaemonCheckReplyReceived(QDBusPendingCallWatcher* watcher, bool apply)
{
    QDBusPendingReply<QStringList> reply = *watcher;

    if (reply.isError())
    {
        emit error(i18n("DBus error while checking config: \'%1\'", reply.error().message()));

        watcher->deleteLater();
        return;
    }

    const auto errors = reply.value();

    if (!errors.isEmpty())
    {
        for (const auto &e : errors)
        {
            emit error(i18n("Error checking config: \'%1\'", e));
        }

        watcher->deleteLater();
        return;
    }

    watcher->deleteLater();

    if (apply)
    {
        auto applyReply = m_dbusInterface->Apply(config());
        watcher = new QDBusPendingCallWatcher(applyReply, this);
        connect(watcher, &QDBusPendingCallWatcher::finished, this, &Loader::onDaemonApplyReplyReceived);
    }
}

QList<QObject *> Loader::hwmonsAsObjects() const
{
    auto list = QList<QObject *>();
    for (const auto &hwmon : qAsConst(m_hwmons))
        list << hwmon;

    return list;
}

VirtualTemp* Loader::virtualTemp(const QString& path)
{
    for (auto virtualTemp : qAsConst(m_virtualTemps))
    {
        if (virtualTemp->path() == path)
            return virtualTemp;
    }

    auto virtualTemp = new VirtualTemp(this, path);
    connect(this, &Loader::sensorsUpdateNeeded, virtualTemp, &VirtualTemp::update);

    m_virtualTemps << virtualTemp;
    emit virtualSensorsChanged(m_virtualTemps);

    return virtualTemp;
}

void Loader::toDefault()
{
    for (const auto &hwmon : qAsConst(m_hwmons))
    {
        hwmon->toDefault();
    }

    m_virtualTemps = QList<VirtualTemp*>();
}

void Loader::addVirtualTemp(const QUrl& url)
{
    virtualTemp(url.toLocalFile());
}

void Loader::removeVirtualTemp(const QUrl& url)
{
    auto path = url.toLocalFile();

    for (auto virtualTemp : qAsConst(m_virtualTemps))
    {
        if (virtualTemp->path() == path)
        {
            for (auto hwmon : qAsConst(m_hwmons))
            {
                const auto pwmFans = hwmon->pwmFans();
                for (const auto pwmFan : pwmFans)
                {
                    if (pwmFan->virtualTemps().contains(virtualTemp))
                    {
                        auto virtualTemps = pwmFan->virtualTemps();
                        virtualTemps.removeAll(virtualTemp);
                        pwmFan->setVirtualTemps(virtualTemps);
                    }
                }
            }

            virtualTemp->deleteLater();
            m_virtualTemps.removeAll(virtualTemp);
            emit virtualSensorsChanged(m_virtualTemps);
        }
    }
}

}
