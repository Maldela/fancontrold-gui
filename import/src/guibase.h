/*
 * Copyright 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef GUIBASE_H
#define GUIBASE_H

#include <QtCore/QObject>
#include <QtCore/QStringListModel>
#include <QtCore/QUrl>

#include "loader.h"
#include "models/profilesmodel.h"
#include "models/pwmfanmodel.h"
#include "models/tempmodel.h"
#include "models/virtualtempmodel.h"
#include "models/fanmodel.h"
#include "models/sensorproxymodel.h"

#ifndef NO_SYSTEMD
#include "systemd/systemdcommunicator.h"
#endif


namespace Fancontrold
{

class Config;

class GUIBase : public QObject
{
    Q_OBJECT

#ifndef NO_SYSTEMD
    Q_PROPERTY(SystemdCommunicator* systemdCom READ systemdCommunicator CONSTANT)
#endif

    Q_PROPERTY(PwmFanModel *pwmFanModel READ pwmFanModel CONSTANT)
    Q_PROPERTY(TempModel *tempModel READ tempModel CONSTANT)
    Q_PROPERTY(VirtualTempModel *virtualTempModel READ virtualTempModel CONSTANT)
    Q_PROPERTY(FanModel *fanModel READ fanModel CONSTANT)
    Q_PROPERTY(SensorSortProxyModel *sensorModel READ sensorModel CONSTANT)
    Q_PROPERTY(SensorSortProxyModel *combinedTempModel READ combinedTempModel CONSTANT)
    Q_PROPERTY(ProfilesModel *profilesModel READ profilesModel CONSTANT)
    Q_PROPERTY(Loader* loader READ loader CONSTANT)
    Q_PROPERTY(QStringList errors READ errors NOTIFY error)
    Q_PROPERTY(QStringList warnings READ warnings NOTIFY warning)
    Q_PROPERTY(QStringList infos READ infos NOTIFY info)
    Q_PROPERTY(bool needsApply READ needsApply NOTIFY needsApplyChanged)
    Q_PROPERTY(bool hasSystemdCommunicator READ hasSystemdCommunicator CONSTANT)

public:

    explicit GUIBase(QObject *parent = nullptr);
    virtual ~GUIBase() {}

    Loader *loader() const { return m_loader; }

#ifndef NO_SYSTEMD
    SystemdCommunicator *systemdCommunicator() const { return m_com; }
#endif

    bool configValid() const { return m_configValid; }
    QStringList errors() const { return m_errors; }
    QStringList warnings() const { return m_warnings; }
    QStringList infos() const { return m_infos; }
    bool needsApply() const;
    PwmFanModel *pwmFanModel() const { return m_pwmFanModel; }
    TempModel *tempModel() const { return m_tempModel; }
    VirtualTempModel *virtualTempModel() const { return m_virtualTempModel; }
    FanModel *fanModel() const { return m_fanModel; }
    SensorSortProxyModel *sensorModel() const { return m_sensorModel; }
    SensorSortProxyModel *combinedTempModel() const { return m_combinedTempModel; }
    ProfilesModel *profilesModel() const { return m_profilesModel; }
    void handleError(const QString &error);
    void handleWarning(const QString &warning);
    void handleInfo(const QString &info);

    Q_INVOKABLE bool hasSystemdCommunicator() const;
    Q_INVOKABLE void apply();
    Q_INVOKABLE void reset();
    Q_INVOKABLE void applyProfile(const QString &profileName);
    Q_INVOKABLE void saveProfile(const QString &profileName, QString profileConfig = QString());
    Q_INVOKABLE void deleteProfile(const QString &profileName);


signals:

    void unitChanged(QString);
    void error(QString);
    void warning(QString);
    void info(QString);
    void needsApplyChanged();


private:

    QStringList m_errors;
    QStringList m_warnings;
    QStringList m_infos;

#ifndef NO_SYSTEMD
    SystemdCommunicator *const m_com;
#endif

    Loader *const m_loader;
    bool m_configValid;
    PwmFanModel *m_pwmFanModel;
    TempModel *m_tempModel;
    VirtualTempModel *m_virtualTempModel;
    FanModel *m_fanModel;
    SensorSortProxyModel *m_sensorModel;
    SensorSortProxyModel *m_combinedTempModel;
    ProfilesModel *m_profilesModel;
};

}

#endif // GUIBASE_H
