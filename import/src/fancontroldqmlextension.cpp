/*
 * Copyright 2016  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "fancontroldqmlextension.h"

#include "models/sensorproxymodel.h"
#include "qml/fangraph.h"
#include "sensors/pwmfan.h"
#include "guibase.h"
#include "config.h"

#include <QtQml/qqml.h>


namespace Fancontrold
{

void FancontroldQmlExtension::registerTypes(const char* uri)
{
    Q_ASSERT(uri == QLatin1String("Fancontrold.Qml"));

    qmlRegisterType<FanGraph>(uri, 1, 0, "FanGraph");
    qmlRegisterUncreatableType<SensorProxyModel>(uri, 1, 0, "SensorProxyModel", QStringLiteral("SensorProxyModel is not instantiable from QML!"));
    qmlRegisterUncreatableType<PwmFan>(uri, 1, 0, "PwmFan", QStringLiteral("PwmFan is not instantiable from QML!"));
    qmlRegisterSingletonType<GUIBase>(uri, 1, 0, "Base", base);
    qmlRegisterSingletonType<Config>(uri, 1, 0, "Config", config);
}

QObject* FancontroldQmlExtension::base(QQmlEngine *engine, QJSEngine *jsengine)
{
    Q_UNUSED(engine)
    Q_UNUSED(jsengine)

    return new GUIBase;
}

QObject* FancontroldQmlExtension::config(QQmlEngine *engine, QJSEngine *jsengine)
{
    Q_UNUSED(engine)
    Q_UNUSED(jsengine)

    return Config::instance();
}

}

