/*
 * Copyright (C) 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


#ifndef LOADER_H
#define LOADER_H

#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QUrl>
#include <QtCore/QJsonDocument>


class OrgKdeFancontroldInterface;
class QDBusPendingCallWatcher;
class QTimer;

namespace Fancontrold
{

class Hwmon;
class PwmFan;
class Temp;
class VirtualTemp;
class Fan;
class GUIBase;

class Loader : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString config READ config NOTIFY configChanged)
    Q_PROPERTY(QString daemonConfig READ daemonConfig NOTIFY daemonConfigChanged)
    Q_PROPERTY(QList<QObject *> hwmons READ hwmonsAsObjects NOTIFY hwmonsChanged)
    Q_PROPERTY(bool needsApply READ needsApply NOTIFY needsApplyChanged)
    Q_PROPERTY(bool daemonConnected READ daemonConnected NOTIFY daemonConnectedChanged)


public:

    explicit Loader(GUIBase *parent = nullptr, bool parseHwmons = true);

    Q_INVOKABLE void apply();
    Q_INVOKABLE void save();
    Q_INVOKABLE void reset();
    Q_INVOKABLE void disable();

    Q_INVOKABLE void addVirtualTemp(const QUrl& url);
    Q_INVOKABLE void removeVirtualTemp(const QUrl& url);

    bool load(const QString &newConfig);
    void connectDBus(QString connection = QString(), QString path = QString());
    void parseHwmons(QString path = QString());
    QList<Hwmon *> hwmons() const { return m_hwmons.values(); }
    QString config() const { return m_config.toJson(); }
    QString daemonConfig() const { return m_daemonConfig.toJson(); }
    Hwmon *hwmon(const QString &hwmon_device_path) const;
    QList<QObject *> hwmonsAsObjects() const;
    PwmFan *pwmFan(uint hwmonIndex, uint pwmFanIndex) const;
    Temp *temp(uint hwmonIndex, uint tempIndex) const;
    VirtualTemp *virtualTemp(const QString& path);
    Fan *fan(uint hwmonIndex, uint fanIndex) const;
    QList<VirtualTemp*> virtualTemps() const { return m_virtualTemps; }
    void toDefault();
    bool needsApply() const { return m_config != m_daemonConfig && m_daemonConnected; }
    void updateConfig();
    bool daemonConnected() const { return m_daemonConnected; }


protected:

    QMap<uint, Hwmon *> m_hwmons;

    bool parseConfig(const QJsonDocument &config);
    QJsonDocument createConfig() const;
    void onDaemonConfigChanged(const QString &daemonConfig);
    void onDaemonError(const QString &e);
    void onServiceRegistered(const QString &serviceName);
    void onServiceUnregistered(const QString &serviceName);
    void updateDaemonConnected();
    void onDaemonCheckReplyReceived(QDBusPendingCallWatcher *watcher, bool apply = false);
    void onDaemonApplyReplyReceived(QDBusPendingCallWatcher *watcher);


private:

    QJsonDocument m_config;
    QJsonDocument m_daemonConfig;
    QTimer *m_timer;
    bool m_daemonConnected;
    OrgKdeFancontroldInterface *m_dbusInterface;
    QList<VirtualTemp*> m_virtualTemps;


signals:

    void configChanged();
    void hwmonsChanged();
    void error(QString) const;
    void warning(QString) const;
    void info(QString) const;
    void sensorsUpdateNeeded();
    void invalidConfigUrl();
    void requestSetServiceActive(bool);
    void needsApplyChanged();
    void daemonConnectedChanged();
    void daemonConfigChanged();
    void virtualSensorsChanged(QList<VirtualTemp*>);
};

}

#endif // LOADER_H
