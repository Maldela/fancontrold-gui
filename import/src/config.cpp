/*
 * Copyright 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "config.h"


#ifndef CONFIG_NAME
#define CONFIG_NAME "fancontrold-gui"
#endif

#ifndef STANDARD_SERVICE_NAME
#define STANDARD_SERVICE_NAME "fancontrold"
#endif


namespace Fancontrold
{

Config *Config::m_instance = nullptr;

Config::Config(QObject *parent) : KCoreConfigSkeleton(KSharedConfig::openConfig(QStringLiteral(CONFIG_NAME)), parent)
{
    setCurrentGroup(QStringLiteral("preferences"));

    addItemDouble(QStringLiteral("MinTemp"), m_minTemp, 30.0);
    addItemDouble(QStringLiteral("MaxTemp"), m_maxTemp, 90.0);
    addItemString(QStringLiteral("ServiceName"), m_serviceName, QStringLiteral(STANDARD_SERVICE_NAME));
    addItemStringList(QStringLiteral("Profiles"), m_profiles, QStringList());
    addItemStringList(QStringLiteral("ProfileNames"), m_profileNames, QStringList());
    addItemInt(QStringLiteral("CurrentProfile"), m_currentProfile, 0);
    addItemBool(QStringLiteral("ShowScales"), m_showScales, true);
    addItemBool(QStringLiteral("ShowTray"), m_showTray, false);
    addItemBool(QStringLiteral("DarkTray"), m_darkTray, true);
    addItemBool(QStringLiteral("StartMinimized"), m_startMinimized, false);

    load();
}

Config::~Config() {
    save();
}

Config* Config::instance()
{
    if (!m_instance)
        m_instance = new Config;

    return m_instance;
}

qreal Config::minTemp() const
{
    return findItem(QStringLiteral("MinTemp"))->property().toReal();
}

void Config::setMinTemp(qreal temp)
{
    if (temp == minTemp())
        return;

    Config::instance()->findItem(QStringLiteral("MinTemp"))->setProperty(temp);
    emit minTempChanged();
}

qreal Config::maxTemp() const
{
    return findItem(QStringLiteral("MaxTemp"))->property().toReal();
}

void Config::setMaxTemp(qreal temp)
{
    if (temp == maxTemp())
        return;

    Config::instance()->findItem(QStringLiteral("MaxTemp"))->setProperty(temp);
    emit maxTempChanged();
}

QString Config::serviceName() const
{
    return findItem(QStringLiteral("ServiceName"))->property().toString();
}

void Config::setServiceName(const QString& name)
{
    if (name == serviceName())
        return;

    findItem(QStringLiteral("ServiceName"))->setProperty(name);
    emit serviceNameChanged();
}

bool Config::showScales() const
{
    return findItem(QStringLiteral("ShowScales"))->property().toBool();
}

void Config::setShowScales(bool showScales)
{
    if (this->showScales() == showScales)
        return;

    findItem(QStringLiteral("ShowScales"))->setProperty(showScales);
    emit showScalesChanged();
}

bool Config::showTray() const
{
    return findItem(QStringLiteral("ShowTray"))->property().toBool();
}

void Config::setShowTray(bool showTray)
{
    if (this->showTray() == showTray)
        return;

    findItem(QStringLiteral("ShowTray"))->setProperty(showTray);
    emit showTrayChanged();
}

bool Config::darkTray() const
{
    return findItem(QStringLiteral("DarkTray"))->property().toBool();
}

void Config::setDarkTray(bool darkTray)
{
    if (this->darkTray() == darkTray)
        return;

    findItem(QStringLiteral("DarkTray"))->setProperty(darkTray);
    emit darkTrayChanged();
}

bool Config::startMinimized() const
{
    return findItem(QStringLiteral("StartMinimized"))->property().toBool();
}

void Config::setStartMinimized(bool sm)
{
    if (startMinimized() == sm)
        return;

    findItem(QStringLiteral("StartMinimized"))->setProperty(sm);
    emit startMinimizedChanged();
}

QMap<QString, QString> Config::profiles() const
{
    auto profiles = findItem(QStringLiteral("Profiles"))->property().toStringList();
    auto profileNames = findItem(QStringLiteral("ProfileNames"))->property().toStringList();

    if (profiles.size() != profileNames.size())
    {
        while (profiles.size() > profileNames.size())
            profiles.removeLast();

        while (profiles.size() < profileNames.size())
            profileNames.removeLast();

        findItem(QStringLiteral("Profiles"))->setProperty(profiles);
        findItem(QStringLiteral("ProfileNames"))->setProperty(profileNames);
    }

    QMap<QString, QString> profilesMap;

    for (int i = 0; i < profileNames.size(); i++)
    {
        profilesMap.insert(profileNames.at(i), profiles.at(i));
    }

    return profilesMap;
}

void Config::setProfiles(const QMap<QString, QString>& newProfiles)
{
    if (newProfiles == profiles())
        return;

    findItem(QStringLiteral("ProfileNames"))->setProperty(QVariant(newProfiles.keys()));
    findItem(QStringLiteral("Profiles"))->setProperty(QVariant(newProfiles.values()));
    emit profilesChanged();
}

}
