/*
 * Copyright 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "virtualtemp.h"

#include "../hwmon.h"
#include "../loader.h"

#include <QtCore/QTextStream>
#include <QtCore/QFile>
#include <QtCore/QDir>

#include <KConfigCore/KSharedConfig>
#include <KConfigCore/KConfigGroup>
#include <KI18n/KLocalizedString>


namespace Fancontrold
{

VirtualTemp::VirtualTemp(Loader *parent, const QString& path) : QObject(parent),
    m_valueStream(new QTextStream),
    m_path(path)
{
    if (!parent)
        return;

    connect(this, &VirtualTemp::error, parent, &Loader::error);

    const auto valueFile = new QFile(path, this);

    if (valueFile->open(QFile::ReadOnly))
    {
        m_valueStream->setDevice(valueFile);
        *m_valueStream >> m_value;
        m_value /= 1000;
    }
    else
    {
        delete valueFile;
        emit error(i18n("Can't open virtual temp file: \'%1\'", path));
    }
}

VirtualTemp::~VirtualTemp()
{
    auto device = m_valueStream->device();
    delete m_valueStream;
    delete device;
}

void VirtualTemp::update()
{
    auto success = false;

    m_valueStream->seek(0);
    const auto value = m_valueStream->readAll().toInt(&success) / 1000;

    if (!success)
        emit error(i18n("Can't update value of virtual temp: \'%1\'", path()));

    if (value != m_value)
    {
        m_value = value;
        emit valueChanged();
    }
}

bool VirtualTemp::isValid() const
{
    return m_valueStream->device() || m_valueStream->string();
}

}
