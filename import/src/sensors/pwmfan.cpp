/*
 * Copyright 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "pwmfan.h"

#include "../hwmon.h"

#include <QtCore/QTextStream>
#include <QtCore/QDir>
#include <QtCore/QFile>

#include <KI18n/KLocalizedString>


#define TEST_HWMON_NAME "test"
#define MAX_ERRORS_FOR_RPM_ZERO 10


namespace Fancontrold
{

PwmFan::PwmFan(uint index, Hwmon *parent, bool device) : Fan(index, parent, device, QStringLiteral("pwm")),
    m_pwmStream(new QTextStream),
    m_pwm(0),
    m_pwmEnable(FullSpeed),
    m_pwmMode(Automatic),
    m_minStart(255),
    m_average(1),
    m_minPwm(100),
    m_mtf(Max),
    m_controlMode(NoControl)
{
    if (!parent)
        return;

    auto path = device ? parent->path() + "/device" : parent->path();

    if (QDir(path).isReadable())
    {
        connect(this, &PwmFan::activeChanged, parent, &Hwmon::configUpdateNeeded);
        connect(this, &PwmFan::tempsChanged, parent, &Hwmon::configUpdateNeeded);
        connect(this, &PwmFan::virtualTempsChanged, parent, &Hwmon::configUpdateNeeded);
        connect(this, &PwmFan::curveChanged, parent, &Hwmon::configUpdateNeeded);
        connect(this, &PwmFan::anchorChanged, parent, &Hwmon::configUpdateNeeded);
        connect(this, &PwmFan::minStartChanged, parent, &Hwmon::configUpdateNeeded);
        connect(this, &PwmFan::averageChanged, parent, &Hwmon::configUpdateNeeded);
        connect(this, &PwmFan::minPwmChanged, parent, &Hwmon::configUpdateNeeded);
        connect(this, &PwmFan::pwmEnableChanged, parent, &Hwmon::configUpdateNeeded);
        connect(this, &PwmFan::pwmModeChanged, parent, &Hwmon::configUpdateNeeded);
        connect(this, &PwmFan::mtfChanged, parent, &Hwmon::configUpdateNeeded);
        connect(this, &PwmFan::controlModeChanged, parent, &Hwmon::configUpdateNeeded);

        const auto pwmFile = new QFile(path + "/pwm" + QString::number(index), this);

        if (pwmFile->open(QFile::ReadWrite))
        {
            m_pwmStream->setDevice(pwmFile);
            PwmFan::setPwm(m_pwmStream->readAll().toInt());
        }
        else if (pwmFile->open(QFile::ReadOnly))
        {
            m_pwmStream->setDevice(pwmFile);
            PwmFan::setPwm(m_pwmStream->readAll().toInt());
        }
        else
        {
            emit error(i18n("Can't open pwm file: \'%1\'", pwmFile->fileName()));
            delete pwmFile;
        }
    }

    connect(this, &PwmFan::mtfChanged, this, &PwmFan::tempChanged);
}

PwmFan::~PwmFan()
{
    auto device = m_pwmStream->device();
    delete m_pwmStream;
    delete device;
}

int PwmFan::temp() const
{
    if (m_temps.isEmpty() && m_virtualTemps.isEmpty())
        return 0;

    switch (m_mtf) {
        case Average:
        {
            int average = 0;
            for (const auto &temp : qAsConst(m_temps))
            {
                average += temp->value();
            }
            for (const auto &temp : qAsConst(m_virtualTemps))
            {
                average += temp->value();
            }
            return average / (m_temps.size() + m_virtualTemps.size());
        }
        case Min:
        {
            int min = INT_MAX;
            for (const auto &temp : qAsConst(m_temps))
            {
                if (temp->value() < min)
                    min = temp->value();
            }
            for (const auto &temp : qAsConst(m_virtualTemps))
            {
                if (temp->value() < min)
                    min = temp->value();
            }
            return min;
        }
        case Max:
        {
            int max = INT_MIN;
            for (const auto &temp : qAsConst(m_temps))
            {
                if (temp->value() > max)
                    max = temp->value();
            }
            for (const auto &temp : qAsConst(m_virtualTemps))
            {
                if (temp->value() > max)
                    max = temp->value();
            }
            return max;
        }
    }

    return 0;
}

void PwmFan::update()
{
    Fan::update();

    m_pwmStream->seek(0);
    setPwm(m_pwmStream->readAll().toInt());
}

void PwmFan::toDefault()
{
    Fan::toDefault();

    setControlMode(NoControl);
    setTemps(QList<Temp*>());
    setVirtualTemps(QList<VirtualTemp*>());
    setCurve(Curve());
    setMinStart(255);
    setMinPwm(100);
    setPwmEnable(FullSpeed);
    setPwmMode(Automatic);
    setMtf(Max);

    if (m_pwmStream->device() && parent())
    {
        auto path = device() ? parent()->path() + "/device" : parent()->path();

        auto device = m_pwmStream->device();
        m_pwmStream->setDevice(nullptr);
        delete device;

        const auto pwmFile = new QFile(path + "/pwm" + QString::number(index()), this);

        if (pwmFile->open(QFile::ReadOnly))
        {
            m_pwmStream->setDevice(pwmFile);
            *m_pwmStream >> m_pwm;
        }
        else
        {
            emit error(i18n("Can't open pwm file: \'%1\'", pwmFile->fileName()));
            delete pwmFile;
        }
    }
}

bool PwmFan::isValid() const
{
    return Fan::isValid() && (m_pwmStream->device() || m_pwmStream->string());
}

QList<QObject*> PwmFan::tempsAsObjects() const
{
    QList<QObject*> objects;
    for (const auto &temp : qAsConst(m_temps))
    {
        objects << temp;
    }
    return objects;
}

QList<QObject*> PwmFan::virtualTempsAsObjects() const
{
    QList<QObject*> objects;
    for (const auto &temp : qAsConst(m_virtualTemps))
    {
        objects << temp;
    }
    return objects;
}

void PwmFan::setTemps(const QList<Temp*> &temps) {
    if (temps == m_temps)
        return;

    for (const auto &temp : qAsConst(m_temps))
    {
        disconnect(temp, &Temp::valueChanged, this, &PwmFan::tempChanged);
    }

    for (const auto &temp : qAsConst(temps))
    {
        connect(temp, &Temp::valueChanged, this, &PwmFan::tempChanged);
    }

    m_temps = temps;
    emit tempsChanged();

    emit tempChanged();
}

void PwmFan::setVirtualTemps(const QList<VirtualTemp*> &temps) {
    if (temps == m_virtualTemps)
        return;

    for (const auto &temp : qAsConst(m_virtualTemps))
    {
        disconnect(temp, &VirtualTemp::valueChanged, this, &PwmFan::tempChanged);
    }

    for (const auto &temp : qAsConst(temps))
    {
        connect(temp, &VirtualTemp::valueChanged, this, &PwmFan::tempChanged);
    }

    m_virtualTemps = temps;
    emit virtualTempsChanged();

    emit tempChanged();
}

void PwmFan::setTempsAsObjects(const QList<QObject*> &tempObjects)
{
    QList<Temp*> temps;
    for (const auto &tempObject : tempObjects)
    {
        if (auto temp = qobject_cast<Temp*>(tempObject))
            temps << temp;
    }
    setTemps(temps);
}

void PwmFan::setVirtualTempsAsObjects(const QList<QObject*> &tempObjects)
{
    QList<VirtualTemp*> temps;
    for (const auto &tempObject : tempObjects)
    {
        if (auto temp = qobject_cast<VirtualTemp*>(tempObject))
            temps << temp;
    }
    setVirtualTemps(temps);
}

void PwmFan::setCurve(const Curve &curve)
{
    auto sort = [] (QPoint point1, QPoint point2)
    {
        return point1.x() < point2.x();
    };

    auto sortedCurve = curve;
    std::sort(sortedCurve.begin(), sortedCurve.end(), sort);

    for (auto point = sortedCurve.begin(); point != sortedCurve.end(); point++)
    {
        point->ry() = qBound(0, point->y(), 255);
    }

    if (m_curve != sortedCurve)
    {
        m_curve = sortedCurve;
        emit curveChanged();
    }
}

void PwmFan::setAnchor(int anchor)
{
    if (m_anchor == anchor)
        return;

    m_anchor = anchor;
    emit anchorChanged();
}

void PwmFan::setAverage(int average)
{
    average = qMax(1, average);

    if (average == m_average)
        return;

    m_average = average;
    emit averageChanged();
}

QVariantList PwmFan::curveAsVariants() const
{
    QVariantList variants;
    for (const auto &point : qAsConst(m_curve))
    {
        variants << point;
    }
    return variants;
}

void PwmFan::setCurve(const QVariantList &curveVariants)
{
    Curve curve;
    for (const auto &pointvariant : curveVariants)
    {
        auto point = pointvariant.toPoint();
        if (!point.isNull())
            curve << point;
    }
    setCurve(curve);
}

void PwmFan::setPwm(int pwm)
{
    pwm = qBound(0, pwm, 255);

    if (pwm == m_pwm)
        return;

    m_pwm = pwm;
    emit pwmChanged();
}

void PwmFan::setPwmEnable(PwmEnable pwmEnable)
{
    if (pwmEnable == m_pwmEnable)
        return;

    m_pwmEnable = pwmEnable;
    emit pwmEnableChanged();
}

void PwmFan::setPwmMode(PwmMode pwmMode)
{
    if (pwmMode == m_pwmMode)
        return;

    m_pwmMode = pwmMode;
    emit pwmModeChanged();
}

void PwmFan::setPwmMode(const QString &pwmModeString)
{
    if (pwmModeString.toLower() == QStringLiteral("dc"))
        setPwmMode(DC);
    else if (pwmModeString.toLower() == QStringLiteral("pwm"))
        setPwmMode(PWM);
    else if (pwmModeString.toLower() == QStringLiteral("automatic"))
        setPwmMode(Automatic);
    else
        emit error("Invalid pwm mode given as string: " + pwmModeString);
}

void PwmFan::setMtf(const QString &mtfString)
{
    if (mtfString.toLower() == QStringLiteral("min"))
        setMtf(Min);
    else if (mtfString.toLower() == QStringLiteral("max"))
        setMtf(Max);
    else if (mtfString.toLower() == QStringLiteral("average"))
        setMtf(Average);
    else
        emit error("Invalid mtf given as string: " + mtfString);
}

void PwmFan::setMinPwm(int minPwm)
{
    minPwm = qBound(0, minPwm, 255);

    if (minPwm == m_minPwm)
        return;

    m_minPwm = minPwm;
    emit minPwmChanged();
}

void PwmFan::setMinStart(int minStart)
{
    minStart = qBound(0, minStart, 255);

    if (minStart == m_minStart)
        return;

    m_minStart = minStart;
    emit minStartChanged();
}

}
