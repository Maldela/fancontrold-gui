/*
 * Copyright 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef PWMFAN_H
#define PWMFAN_H


#include "temp.h"
#include "virtualtemp.h"
#include "fan.h"

#include <QPoint>


class QTextStream;

namespace Fancontrold
{

typedef QList<QPoint> Curve;

class PwmFan : public Fan
{
    Q_OBJECT
    Q_PROPERTY(int pwm READ pwm NOTIFY pwmChanged)
    Q_PROPERTY(int temp READ temp NOTIFY tempChanged)
    Q_PROPERTY(QList<QObject*> temps READ tempsAsObjects WRITE setTempsAsObjects NOTIFY tempsChanged)
    Q_PROPERTY(QList<QObject*> virtualTemps READ virtualTempsAsObjects WRITE setVirtualTempsAsObjects NOTIFY virtualTempsChanged)
    Q_PROPERTY(QVariantList curve READ curveAsVariants WRITE setCurve NOTIFY curveChanged)
    Q_PROPERTY(int anchor READ anchor WRITE setAnchor NOTIFY anchorChanged)
    Q_PROPERTY(int minStart READ minStart WRITE setMinStart NOTIFY minStartChanged)
    Q_PROPERTY(int average READ average WRITE setAverage NOTIFY averageChanged)
    Q_PROPERTY(int minPwm READ minPwm WRITE setMinPwm NOTIFY minPwmChanged)
    Q_PROPERTY(MultiTempFunction mtf READ mtf WRITE setMtf NOTIFY mtfChanged)
    Q_PROPERTY(PwmEnable pwmEnable READ pwmEnable NOTIFY pwmEnableChanged)
    Q_PROPERTY(PwmMode pwmMode READ pwmMode WRITE setPwmMode NOTIFY pwmModeChanged)
    Q_PROPERTY(ControlMode controlMode READ controlMode WRITE setControlMode NOTIFY controlModeChanged)

public:

    enum PwmEnable
    {
        FullSpeed = 0,
        ManualControl = 1,
        BiosControl = 2
    };
    Q_ENUM(PwmEnable)

    enum PwmMode
    {
        DC = 0,
        PWM = 1,
        Automatic = 2
    };
    Q_ENUM(PwmMode)

    enum ControlMode
    {
        CurveControl = 0,
        AnchorControl = 1,
        NoControl = 2
    };
    Q_ENUM(ControlMode)

    enum MultiTempFunction
    {
        Min,
        Max,
        Average
    };
    Q_ENUM(MultiTempFunction)

    explicit PwmFan(uint index, Hwmon *parent, bool device = false);
    virtual ~PwmFan();

    virtual QString type() const override { return "pwm"; }
    int pwm() const override { return m_pwm; }
    int temp() const;
    QList<Temp*> temps() const { return m_temps; }
    QList<VirtualTemp*> virtualTemps() const { return m_virtualTemps; }
    QList<QObject*> tempsAsObjects() const;
    QList<QObject*> virtualTempsAsObjects() const;
    Curve curve() const { return m_curve; }
    int anchor() const { return m_anchor; }
    QVariantList curveAsVariants() const;
    int average() const { return m_average; }
    PwmEnable pwmEnable() const { return m_pwmEnable; }
    PwmMode pwmMode() const { return m_pwmMode; }
    MultiTempFunction mtf() const { return m_mtf; }
    ControlMode controlMode() const { return m_controlMode; }
    int minStart() const { return m_minStart; }
    int minPwm() const { return m_minPwm; }
    void setTemps(const QList<Temp*> &temps);
    void setVirtualTemps(const QList<VirtualTemp*> &temps);
    void setTempsAsObjects(const QList<QObject*> &tempObjects);
    void setVirtualTempsAsObjects(const QList<QObject*> &tempObjects);
    void setCurve(const Curve &curve);
    void setCurve(const QVariantList &curveVariants);
    void setAnchor(int anchor);
    void setAverage(int average);
    void setMtf(MultiTempFunction mtf) { if (mtf != m_mtf) { m_mtf = mtf; emit mtfChanged(); emit tempChanged(); } }
    void setControlMode(ControlMode controlMode) { if (m_controlMode != controlMode) { m_controlMode = controlMode; emit controlModeChanged(); } }
    void setMtf(const QString &mtfString);
    void setMinStart(int minStart);
    void setMinPwm(int minPwm);
    void setPwmMode(PwmMode pwmMode);
    void setPwmMode(const QString &pwmModeString);
    void toDefault() override;
    bool isValid() const override;
    void update() override;


signals:

    void pwmChanged();
    void tempChanged();
    void tempsChanged();
    void virtualTempsChanged();
    void curveChanged();
    void anchorChanged();
    void averageChanged();
    void activeChanged();
    void pwmEnableChanged();
    void pwmModeChanged();
    void mtfChanged();
    void minStartChanged();
    void minPwmChanged();
    void controlModeChanged();


protected:

    void setPwm(int pwm);
    void setPwmEnable(PwmEnable pwmEnable);

    QTextStream *m_pwmStream;


private:

    int m_pwm;
    PwmEnable m_pwmEnable;
    PwmMode m_pwmMode;
    QList<Temp*> m_temps;
    QList<VirtualTemp*> m_virtualTemps;
    Curve m_curve;
    int m_anchor;
    int m_minStart;
    int m_average;
    int m_minPwm;
    MultiTempFunction m_mtf;
    ControlMode m_controlMode;
};

}

#endif // PWMFAN_H
