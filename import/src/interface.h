#ifndef INTERFACE_H
#define INTERFACE_H

#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QVariant>

#include <QtDBus/QDBusAbstractInterface>
#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusPendingReply>

#ifndef DAEMON_DBUS_INTERFACE_NAME
#define DAEMON_DBUS_INTERFACE_NAME "org.kde.fancontrold"
#endif


class OrgKdeFancontroldInterface: public QDBusAbstractInterface
{
    Q_OBJECT
    Q_PROPERTY(QString Config READ config NOTIFY NewConfig)
    Q_PROPERTY(QString ConfigPath READ configPath CONSTANT)

public:

    static inline const char *staticInterfaceName() { return DAEMON_DBUS_INTERFACE_NAME; }

    explicit OrgKdeFancontroldInterface(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent = nullptr);
    virtual ~OrgKdeFancontroldInterface();

    inline QString config() const { return qvariant_cast<QString>(property("Config")); }
    inline QString configPath() const { return qvariant_cast<QString>(property("ConfigPath")); }

    inline bool isConnected()
    {
        return connection().isConnected();
    }

    inline QDBusPendingReply<> Apply(const QString &config)
    {
        return asyncCall(QStringLiteral("Apply"), config);
    }

    inline QDBusPendingReply<> Load(const QString &path)
    {
        return asyncCall(QStringLiteral("Load"), path);
    }

    inline QDBusPendingReply<> Reload()
    {
        return asyncCall(QStringLiteral("Reload"));
    }

    inline QDBusPendingReply<QStringList> Check(const QString &config)
    {
        return asyncCall(QStringLiteral("Check"), config);
    }


signals:

    void Starting();
    void Quitting();
    void NewConfig(const QString &config);
    void Error(const QString &error);
};

#endif
