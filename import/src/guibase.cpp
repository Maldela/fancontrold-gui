/*
 * Copyright 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "guibase.h"

#include "config.h"
#include "hwmon.h"
#include "sensors/virtualtemp.h"

#include <QtCore/QDebug>

#include <KI18n/KLocalizedString>


namespace Fancontrold
{

GUIBase::GUIBase(QObject *parent) : QObject(parent),

#ifndef NO_SYSTEMD
    m_com(new SystemdCommunicator(this)),
#endif

    m_loader(new Loader(this)),
    m_configValid(false),
    m_pwmFanModel(new PwmFanModel(this)),
    m_tempModel(new TempModel(this)),
    m_virtualTempModel(new VirtualTempModel(this)),
    m_fanModel(new FanModel(this)),
    m_sensorModel(new SensorSortProxyModel(this)),
    m_combinedTempModel(new SensorSortProxyModel(this)),
    m_profilesModel(new ProfilesModel(this))
{
    connect(m_loader, &Loader::needsApplyChanged, this, &GUIBase::needsApplyChanged);

#ifndef NO_SYSTEMD
    connect(m_loader, &Loader::requestSetServiceActive, m_com, &SystemdCommunicator::setServiceActive);
#endif

    m_sensorModel->addSourceModel(m_fanModel);
    m_sensorModel->addSourceModel(m_tempModel);
    m_sensorModel->addSourceModel(m_virtualTempModel);

    m_combinedTempModel->addSourceModel(m_tempModel);
    m_combinedTempModel->addSourceModel(m_virtualTempModel);

    const auto hwmons = m_loader->hwmons();
    QList<Temp*> temps;
    QList<Fan *> fans;
    QList<PwmFan*> pwmFans;
    for (const auto &hwmon : hwmons)
    {
        pwmFans << hwmon->pwmFans().values();
        temps << hwmon->temps().values();
        fans << hwmon->fans().values();
    }
    m_pwmFanModel->setPwmFans(pwmFans);
    m_tempModel->setTemps(temps);
    m_virtualTempModel->setVirtualTemps(m_loader->virtualTemps());
    m_fanModel->setFans(fans);

    connect(m_loader, &Loader::virtualSensorsChanged, m_virtualTempModel, &VirtualTempModel::setVirtualTemps);
}

bool GUIBase::needsApply() const
{
    return m_loader->needsApply();
}

bool GUIBase::hasSystemdCommunicator() const
{
#ifndef NO_SYSTEMD
    return true;
#else
    return false;
#endif
}

void GUIBase::apply()
{
    qInfo() << i18n("Applying changes");
    m_loader->apply();

    qInfo() << i18n("Saving changes to disk");
    m_loader->save();

    Config::instance()->save();

    emit needsApplyChanged();
}

void GUIBase::reset()
{
    qInfo() << i18n("Resetting changes");

    m_loader->reset();

    emit needsApplyChanged();
}

void GUIBase::handleError(const QString &e)
{
    if (e.isEmpty())
        return;

    if (!m_errors.isEmpty() && e == m_errors.last())
        return;

    m_errors << e;
    emit error(e);

    qCritical() << e;
}

void GUIBase::handleWarning(const QString &w)
{
    if (w.isEmpty())
        return;

    if (!m_warnings.isEmpty() && w == m_warnings.last())
        return;

    m_warnings << w;
    emit warning(w);

    qWarning() << w;
}

void GUIBase::handleInfo(const QString &i)
{
    if (i.isEmpty())
        return;

    if (!m_infos.isEmpty() && i == m_infos.last())
        return;

    m_infos << i;
    emit info(i);

    qInfo() << i;
}

void GUIBase::applyProfile(const QString& profileName)
{
    auto profiles = Config::instance()->profiles();

    if (!profiles.contains(profileName))
    {
        handleError(i18n("Unable to apply unknown profile: %1", profileName));
        return;
    }

    auto newConfig = profiles.value(profileName);

    if (m_loader->config() == newConfig)
        return;

    m_loader->load(newConfig);
}

void GUIBase::saveProfile(const QString& profileName, QString profileConfig)
{
    if (profileConfig.isEmpty())
        profileConfig = m_loader->config();

    auto profiles = Config::instance()->profiles();

    profiles.insert(profileName, profileConfig);

    Config::instance()->setProfiles(profiles);
}

void GUIBase::deleteProfile(const QString& profileName)
{
    auto profiles = Config::instance()->profiles();

    if (!profiles.contains(profileName))
    {
        handleError(i18n("Unable to delete unknown profile: %1", profileName));
        return;
    }

    profiles.remove(profileName);

    Config::instance()->setProfiles(profiles);
}

}
