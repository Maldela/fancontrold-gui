/*
 * Copyright (C) 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


#include "systemdcommunicator.h"

#include "managerinterface.h"
#include "unitinterface.h"

#include "../guibase.h"
#include "../config.h"

#include <QtCore/QTimer>
#include <QtDBus/QDBusArgument>
#include <QtDBus/QDBusInterface>
#include <QtDBus/QDBusMetaType>
#include <QtDBus/QDBusReply>
#include <QtDBus/QDBusVariant>

#include <KI18n/KLocalizedString>


namespace Fancontrold
{

SystemdCommunicator::SystemdCommunicator(GUIBase *parent) : QObject(parent),
    m_managerInterface(new OrgFreedesktopSystemd1ManagerInterface(this)),
    m_unitInterface(nullptr)
{
    connect(m_managerInterface, &OrgFreedesktopSystemd1ManagerInterface::UnitFilesChanged, this, &SystemdCommunicator::serviceEnabledChanged);
    connect(m_managerInterface, &OrgFreedesktopSystemd1ManagerInterface::JobRemoved, this, &SystemdCommunicator::onJobRemoved);

    if (parent)
    {
        connect(this, &SystemdCommunicator::error, parent, &GUIBase::handleError);
        connect(this, &SystemdCommunicator::info, parent, &GUIBase::handleInfo);
    }

    auto config = Config::instance();
    setServiceName(config->serviceName());
    connect(config, &Config::serviceNameChanged, this, [=] () { this->setServiceName(config->serviceName()); });
}

void SystemdCommunicator::setServiceName(const QString &name)
{
    if (name == m_serviceName)
        return;

    if (m_unitInterface)
    {
        m_unitInterface->deleteLater();
        m_unitInterface = nullptr;
    }

    m_serviceName = name;
    emit serviceNameChanged();
    emit info(i18n("New service name: \'%1\'", m_serviceName));

    if (serviceExists())
    {
        auto dbusreply = m_managerInterface->LoadUnit(m_serviceName + ".service");
        dbusreply.waitForFinished();

        if (dbusreply.isError())
        {
            emit error(i18n("DBus error: \'%1\'", dbusreply.error().message()));
            m_serviceObjectPath.setPath("");
        }
        else
        {
            m_serviceObjectPath = dbusreply.value();

            createUnitInterface();
        }
    }

    emit serviceEnabledChanged();
    emit serviceActiveChanged();
}

bool SystemdCommunicator::serviceExists() const
{
    if (m_unitInterface && m_unitInterface->isValid())
        return true;

    auto dbusreply = m_managerInterface->ListUnitFiles();
    dbusreply.waitForFinished();

    if (dbusreply.isError())
    {
        emit error(dbusreply.error().message());
        return false;
    }

    const auto list = dbusreply.value();
    for (const auto &unitFile : list)
    {
        if (unitFile.path.contains(m_serviceName + ".service"))
            return true;
    }

    emit error(i18n("Service does not exist: \'%1\'", m_serviceName));
    return false;
}

bool SystemdCommunicator::serviceActive() const
{
    if (m_unitInterface)
    {
        if (m_unitInterface->property("ActiveState").toString() == QStringLiteral("active"))
            return true;
    }

    return false;
}

bool SystemdCommunicator::serviceEnabled() const
{
    if (m_managerInterface->GetUnitFileState(m_serviceName + ".service") == QStringLiteral("enabled"))
        return true;

    return false;
}

void SystemdCommunicator::createUnitInterface()
{
    if (m_unitInterface)
        m_unitInterface->deleteLater();

    m_unitInterface = new OrgFreedesktopSystemd1UnitInterface(m_serviceObjectPath, this);

    if (!m_unitInterface->isValid())
        emit error(i18n("Unable to init systemd dbus unit interface: %1", m_unitInterface->lastError().message()), true);

    emit serviceActiveChanged();
}

void SystemdCommunicator::onJobRemoved(uint id, const QDBusObjectPath& busPath, const QString& unit, const QString &result)
{
    Q_UNUSED(id)
    Q_UNUSED(busPath)
    Q_UNUSED(result)

    if (unit == m_serviceName + ".service")
    {
        emit serviceActiveChanged();
    }
}

void SystemdCommunicator::setServiceActive(bool activated)
{
    if (activated == serviceActive())
        return;

    auto serviceFileName = m_serviceName + ".service";

    if (activated)
    {
        emit info(i18n("Starting service: \'%1\'", m_serviceName));

        auto reply = m_managerInterface->StartUnit(serviceFileName, QStringLiteral("replace"));
        reply.waitForFinished();

        if (reply.isError())
            emit error(reply.error().message(), true);
    }
    else
    {
        emit info(i18n("Stopping service: \'%1\'", m_serviceName));

        auto reply = m_managerInterface->StopUnit(serviceFileName, QStringLiteral("replace"));
        reply.waitForFinished();

        if (reply.isError())
            emit error(reply.error().message(), true);
    }
}

void SystemdCommunicator::setServiceEnabled(bool enabled)
{
    if (enabled == serviceEnabled())
        return;

    auto serviceFileNameList = QStringList() << m_serviceName + ".service";

    if (enabled)
    {
        emit info(i18n("Enabling service autostart at boot: \'%1\'", m_serviceName));

        auto reply = m_managerInterface->EnableUnitFiles(serviceFileNameList, false, true);
        reply.waitForFinished();

        if (reply.isError())
            emit error(reply.error().message(), true);
    }
    else
    {
        emit info(i18n("Disabling service autostart at boot: \'%1\'", m_serviceName));

        auto reply = m_managerInterface->DisableUnitFiles(serviceFileNameList, false);
        reply.waitForFinished();

        if (reply.isError())
            emit error(reply.error().message(), true);
    }
}

}
