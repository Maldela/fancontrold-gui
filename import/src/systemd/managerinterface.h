/*
 * Copyright (C) 2020  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


#ifndef MANAGERINTERFACE_H
#define MANAGERINTERFACE_H

#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QVariant>

#include <QtDBus/QDBusAbstractInterface>
#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusObjectPath>
#include <QtDBus/QDBusPendingReply>


typedef struct
{
    QString path;
    QString state;
} SystemdUnitFile;

Q_DECLARE_METATYPE(SystemdUnitFile)

typedef QList<SystemdUnitFile> SystemdUnitFileList;

Q_DECLARE_METATYPE(SystemdUnitFileList)

typedef struct
{
    QString changeType;
    QString fileName;
    QString destination;
} UnitEnableChange;

Q_DECLARE_METATYPE(UnitEnableChange)

typedef QList<UnitEnableChange> UnitEnableChangeList;

Q_DECLARE_METATYPE(UnitEnableChangeList)


QDBusArgument& operator <<(QDBusArgument &argument, const SystemdUnitFile &unitFile);
const QDBusArgument& operator >>(const QDBusArgument &argument, SystemdUnitFile &unitFile);
QDBusArgument& operator <<(QDBusArgument &argument, const UnitEnableChange &unitEnableChange);
const QDBusArgument& operator >>(const QDBusArgument &argument, UnitEnableChange &unitEnableChange);


namespace Fancontrold
{

class OrgFreedesktopSystemd1ManagerInterface: public QDBusAbstractInterface
{
    Q_OBJECT

public:

    static inline const char *staticInterfaceName() { return "org.freedesktop.systemd1.Manager"; }
    static inline const char *staticServiceName() { return "org.freedesktop.systemd1"; }
    static inline const char *staticNodePath() { return "/org/freedesktop/systemd1"; }

    OrgFreedesktopSystemd1ManagerInterface(QObject *parent = nullptr);
    ~OrgFreedesktopSystemd1ManagerInterface();

    inline QString config() const { return qvariant_cast<QString>(property("Config")); }
    inline QString configPath() const { return qvariant_cast<QString>(property("ConfigPath")); }


    inline QDBusPendingReply<QDBusObjectPath> StartUnit(const QString &unit, const QString &mode)
    {
        return asyncCall(QStringLiteral("StartUnit"), unit, mode);
    }

    inline QDBusPendingReply<QDBusObjectPath> StopUnit(const QString &unit, const QString &mode)
    {
        return asyncCall(QStringLiteral("StopUnit"), unit, mode);
    }

    inline QDBusPendingReply<QString> GetUnitFileState(const QString &unit)
    {
        return asyncCall(QStringLiteral("GetUnitFileState"), unit);
    }

    inline QDBusPendingReply<bool, UnitEnableChangeList> EnableUnitFiles(const QStringList &unitFiles, bool runtimeOnly, bool replace)
    {
        return asyncCall(QStringLiteral("EnableUnitFiles"), unitFiles, runtimeOnly, replace);
    }

    inline QDBusPendingReply<UnitEnableChangeList> DisableUnitFiles(const QStringList &unitFiles, bool runtimeOnly)
    {
        return asyncCall(QStringLiteral("DisableUnitFiles"), unitFiles, runtimeOnly);
    }

    inline QDBusPendingReply<QDBusObjectPath> GetUnit(const QString &unit)
    {
        return asyncCall(QStringLiteral("GetUnit"), unit);
    }

    inline QDBusPendingReply<QDBusObjectPath> LoadUnit(const QString &unit)
    {
        return asyncCall(QStringLiteral("LoadUnit"), unit);
    }

    inline QDBusPendingReply<SystemdUnitFileList> ListUnitFiles()
    {
        return asyncCall(QStringLiteral("ListUnitFiles"));
    }

    inline QDBusPendingReply<> Subscribe()
    {
        return asyncCall(QStringLiteral("Subscribe"));
    }


signals:

    void UnitFilesChanged();
    void JobRemoved(uint id, QDBusObjectPath busPath, QString unit, QString result);
};

}

#endif
