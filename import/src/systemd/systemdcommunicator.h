/*
 * Copyright (C) 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


#ifndef SYSTEMDCOMMUNICATOR_H
#define SYSTEMDCOMMUNICATOR_H

#include <QtCore/QObject>

#include <QtDBus/QDBusObjectPath>


class QDBusInterface;


namespace Fancontrold
{

class GUIBase;
class OrgFreedesktopSystemd1ManagerInterface;
class OrgFreedesktopSystemd1UnitInterface;


class SystemdCommunicator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool serviceExists READ serviceExists NOTIFY serviceNameChanged)
    Q_PROPERTY(bool serviceEnabled READ serviceEnabled WRITE setServiceEnabled NOTIFY serviceEnabledChanged)
    Q_PROPERTY(bool serviceActive READ serviceActive WRITE setServiceActive NOTIFY serviceActiveChanged)

public:

    explicit SystemdCommunicator(GUIBase *parent = nullptr);

    QString serviceName() const { return m_serviceName; }
    void setServiceName(const QString &name);
    bool serviceExists() const;
    bool serviceEnabled() const;
    bool serviceActive() const;
    void setServiceEnabled(bool enabled);
    void setServiceActive(bool active);


signals:

    void serviceNameChanged();
    void serviceEnabledChanged();
    void serviceActiveChanged();
    void error(QString, bool = false) const;
    void info(QString) const;


protected:

    void createUnitInterface();
    void onJobRemoved(uint id, const QDBusObjectPath &busPath, const QString &unit, const QString &result);


private:

    QString m_serviceName;
    QDBusObjectPath m_serviceObjectPath;
    OrgFreedesktopSystemd1ManagerInterface * const m_managerInterface;
    OrgFreedesktopSystemd1UnitInterface *m_unitInterface;
};

}

#endif // SYSTEMDCOMMUNICATOR_H
