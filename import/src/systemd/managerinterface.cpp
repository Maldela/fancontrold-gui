#include "managerinterface.h"

#include <QtDBus/QDBusMetaType>


QDBusArgument& operator <<(QDBusArgument &argument, const SystemdUnitFile &unitFile)
{
    argument.beginStructure();
    argument << unitFile.path << unitFile.state;
    argument.endStructure();

    return argument;
}

const QDBusArgument& operator >>(const QDBusArgument &argument, SystemdUnitFile &unitFile)
{
    argument.beginStructure();
    argument >> unitFile.path >> unitFile.state;
    argument.endStructure();

    return argument;
}

QDBusArgument& operator <<(QDBusArgument &argument, const UnitEnableChange &unitEnableChange)
{
    argument.beginStructure();
    argument << unitEnableChange.changeType << unitEnableChange.fileName << unitEnableChange.destination;
    argument.endStructure();

    return argument;
}

const QDBusArgument& operator >>(const QDBusArgument &argument, UnitEnableChange &unitEnableChange)
{
    argument.beginStructure();
    argument >> unitEnableChange.changeType >> unitEnableChange.fileName >> unitEnableChange.destination;
    argument.endStructure();

    return argument;
}


namespace Fancontrold
{

OrgFreedesktopSystemd1ManagerInterface::OrgFreedesktopSystemd1ManagerInterface(QObject *parent)
    : QDBusAbstractInterface(staticServiceName(), staticNodePath(), staticInterfaceName(), QDBusConnection::systemBus(), parent)
{
    qDBusRegisterMetaType<SystemdUnitFile>();
    qDBusRegisterMetaType<SystemdUnitFileList>();
    qDBusRegisterMetaType<UnitEnableChange>();
    qDBusRegisterMetaType<UnitEnableChangeList>();

    Subscribe();
}

OrgFreedesktopSystemd1ManagerInterface::~OrgFreedesktopSystemd1ManagerInterface()
{
}

}
