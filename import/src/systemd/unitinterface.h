/*
 * Copyright (C) 2020  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


#ifndef UNITINTERFACE_H
#define UNITINTERFACE_H

#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QVariant>

#include <QtDBus/QDBusAbstractInterface>
#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusObjectPath>
#include <QtDBus/QDBusPendingReply>


namespace Fancontrold
{

class OrgFreedesktopSystemd1UnitInterface: public QDBusAbstractInterface
{
    Q_OBJECT
    Q_PROPERTY(QString ActiveState READ activeState)

public:

    static inline const char *staticInterfaceName() { return "org.freedesktop.systemd1.Unit"; }
    static inline const char *staticServiceName() { return "org.freedesktop.systemd1"; }

    OrgFreedesktopSystemd1UnitInterface(const QDBusObjectPath &path, QObject *parent = nullptr);
    ~OrgFreedesktopSystemd1UnitInterface();

    inline QString activeState() const { return qvariant_cast<QString>(property("ActiveState")); }


    inline QDBusPendingReply<QDBusObjectPath> Start(const QString &mode)
    {
        return asyncCall(QStringLiteral("Start"), mode);
    }

    inline QDBusPendingReply<QDBusObjectPath> Stop(const QString &mode)
    {
        return asyncCall(QStringLiteral("Stop"), mode);
    }

    inline QDBusPendingReply<QDBusObjectPath> Reload(const QString &mode)
    {
        return asyncCall(QStringLiteral("Reload"), mode);
    }


private:

    QDBusObjectPath m_objectPath;

};

}

#endif
