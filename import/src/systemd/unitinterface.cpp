#include "unitinterface.h"

#include <QDebug>

namespace Fancontrold
{

OrgFreedesktopSystemd1UnitInterface::OrgFreedesktopSystemd1UnitInterface(const QDBusObjectPath &path, QObject *parent)
    : QDBusAbstractInterface(staticServiceName(), path.path(), staticInterfaceName(), QDBusConnection::systemBus(), parent)
    , m_objectPath(path)
{
}

OrgFreedesktopSystemd1UnitInterface::~OrgFreedesktopSystemd1UnitInterface()
{
}

}
