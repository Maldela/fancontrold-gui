/*
 * Copyright 2021  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fangraph.h"
#include "../sensors/pwmfan.h"

#include <QtGui/QPalette>
#include <QtQuick/QSGClipNode>
#include <QtQuick/QSGGeometryNode>
#include <QtQuick/QSGFlatColorMaterial>
#include <QtQuick/QSGVertexColorMaterial>


namespace Fancontrold
{

FanGraph::FanGraph(QQuickItem *parent) : QQuickItem(parent),
    m_verticalScalaCount(5),
    m_minTemp(20),
    m_maxTemp(100),
    m_horMeshSpacing(10),
    m_borderWidth(2.0),
    m_borderColor(Qt::white),
    m_meshColor(Qt::white),
    m_anchorColor(Qt::white),
    m_fan(nullptr),
    m_backgroundNode(nullptr),
    m_borderNode(nullptr),
    m_curveNode(nullptr),
    m_anchorNode(nullptr),
    m_meshNode(nullptr),
    m_meshChanged(true),
    m_curveChanged(true),
    m_borderChanged(true)
{
    setFlag(ItemHasContents);
    setAntialiasing(true);

    auto palette = QPalette();

    setBorderColor(palette.text().color());
    setBackgroundColor(palette.base().color());

    auto meshColor = palette.text().color();
    meshColor.setAlpha(75);
    setMeshColor(meshColor);

    auto anchorColor = palette.text().color();
    anchorColor.setAlpha(230);
    setAnchorColor(anchorColor);

    connect(this, &FanGraph::backgroundColorChanged, this, &QQuickItem::update);
    connect(this, &FanGraph::anchorColorChanged, this, &QQuickItem::update);
    connect(this, &FanGraph::fanChanged, this, &QQuickItem::update);

    connect(this, &FanGraph::minTempChanged, this, &FanGraph::meshTempsChanged);
    connect(this, &FanGraph::maxTempChanged, this, &FanGraph::meshTempsChanged);
    connect(this, &FanGraph::horMeshSpacingChanged, this, &FanGraph::meshTempsChanged);

    connect(this, &FanGraph::meshTempsChanged, this, &FanGraph::updateMesh);
    connect(this, &QQuickItem::widthChanged, this, &FanGraph::updateMesh);
    connect(this, &QQuickItem::heightChanged, this, &FanGraph::updateMesh);
    connect(this, &FanGraph::meshColorChanged, this, &FanGraph::updateMesh);
    connect(this, &FanGraph::verticalScalaCountChanged, this, &FanGraph::updateMesh);

    connect(this, &QQuickItem::widthChanged, this, &FanGraph::curvePointPositionsChanged);
    connect(this, &QQuickItem::heightChanged, this, &FanGraph::curvePointPositionsChanged);
    connect(this, &FanGraph::minTempChanged, this, &FanGraph::curvePointPositionsChanged);
    connect(this, &FanGraph::maxTempChanged, this, &FanGraph::curvePointPositionsChanged);
    connect(this, &FanGraph::fanChanged, this, &FanGraph::curvePointPositionsChanged);

    connect(this, &FanGraph::curvePointPositionsChanged, this, &FanGraph::updateCurve);

    connect(this, &QQuickItem::widthChanged, this, &FanGraph::statusPointPositionChanged);
    connect(this, &QQuickItem::heightChanged, this, &FanGraph::statusPointPositionChanged);
    connect(this, &FanGraph::minTempChanged, this, &FanGraph::statusPointPositionChanged);
    connect(this, &FanGraph::maxTempChanged, this, &FanGraph::statusPointPositionChanged);
    connect(this, &FanGraph::fanChanged, this, &FanGraph::statusPointPositionChanged);

    connect(this, &QQuickItem::widthChanged, this, &FanGraph::updateBorder);
    connect(this, &QQuickItem::heightChanged, this, &FanGraph::updateBorder);
    connect(this, &FanGraph::borderColorChanged, this, &FanGraph::updateBorder);
    connect(this, &FanGraph::borderWidthChanged, this, &FanGraph::updateBorder);
}

FanGraph::~FanGraph()
{
}

QSGNode* FanGraph::updatePaintNode(QSGNode* oldNode, QQuickItem::UpdatePaintNodeData* data)
{
    Q_UNUSED(data)

    auto root = static_cast<QSGClipNode*>(oldNode);

    if (!root)
        root = new QSGClipNode;
    else
        root->removeAllChildNodes();

    root->setClipRect(boundingRect());
    root->setIsRectangular(true);

    updateBackgroundNode();

    if (m_backgroundNode)
        root->appendChildNode(m_backgroundNode);

    updateMeshNode();

    if (m_meshNode)
        root->appendChildNode(m_meshNode);

    if (m_fan && m_fan->controlMode() == PwmFan::CurveControl)
    {
        updateCurveNode();

        if (m_curveNode)
            root->appendChildNode(m_curveNode);
    }

    if (m_fan && m_fan->controlMode() == PwmFan::AnchorControl)
    {
        updateAnchorNode();

        if (m_anchorNode)
            root->appendChildNode(m_anchorNode);
    }

    updateBorderNode();

    if (m_borderNode)
        root->appendChildNode(m_borderNode);

    return root;
}

void FanGraph::updateBackgroundNode()
{
    if (!m_backgroundNode)
        m_backgroundNode = new QSGGeometryNode;

    auto geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
    geometry->setDrawingMode(QSGGeometry::DrawTriangleStrip);
    geometry->vertexDataAsPoint2D()[0].set(0, 0);
    geometry->vertexDataAsPoint2D()[1].set(width(), 0);
    geometry->vertexDataAsPoint2D()[2].set(0, height());
    geometry->vertexDataAsPoint2D()[3].set(width(), height());

    auto material = new QSGFlatColorMaterial;
    material->setColor(m_backgroundColor);

    m_backgroundNode->setGeometry(geometry);
    m_backgroundNode->setFlag(QSGNode::OwnsGeometry);
    m_backgroundNode->setMaterial(material);
    m_backgroundNode->setFlag(QSGNode::OwnsMaterial);
}

void FanGraph::updateBorderNode()
{
    if (!m_borderChanged)
        return;

    if (!m_borderNode)
        m_borderNode = new QSGGeometryNode;

    qreal halfBorderWidth = m_borderWidth / 2;

    auto geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
    geometry->setDrawingMode(QSGGeometry::DrawLineLoop);
    geometry->setLineWidth(m_borderWidth);
    geometry->vertexDataAsPoint2D()[0].set(halfBorderWidth, halfBorderWidth);
    geometry->vertexDataAsPoint2D()[1].set(width() - halfBorderWidth, halfBorderWidth);
    geometry->vertexDataAsPoint2D()[2].set(width() - halfBorderWidth, height() - halfBorderWidth);
    geometry->vertexDataAsPoint2D()[3].set(halfBorderWidth, height() - halfBorderWidth);

    auto material = new QSGFlatColorMaterial;
    material->setColor(m_borderColor);

    m_borderNode->setGeometry(geometry);
    m_borderNode->setFlag(QSGNode::OwnsGeometry);
    m_borderNode->setMaterial(material);
    m_borderNode->setFlag(QSGNode::OwnsMaterial);

    m_borderChanged = false;
}

void FanGraph::updateMeshNode()
{
    if (!m_meshChanged)
        return;

    if (!m_meshNode)
        m_meshNode = new QSGNode;

    while (auto firstChildNode = m_meshNode->firstChild())
    {
        m_meshNode->removeChildNode(firstChildNode);
        delete firstChildNode;
    }

    auto rect = borderedRect();

    // horizontal lines
    if (m_verticalScalaCount >= 3)
    {
        for (qreal y = rect.y(); y < rect.bottom(); y += rect.height() / (m_verticalScalaCount - 1))
        {
            if (y > rect.y() && y < rect.bottom())
            {
                auto material = new QSGFlatColorMaterial;
                material->setColor(m_meshColor);

                auto geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 2);
                geometry->setDrawingMode(QSGGeometry::DrawLines);
                geometry->setLineWidth(1);
                geometry->vertexDataAsPoint2D()[0].set(rect.left(), y);
                geometry->vertexDataAsPoint2D()[1].set(rect.right(), y);

                auto node = new QSGGeometryNode;
                node->setFlag(QSGNode::OwnedByParent);
                node->setMaterial(material);
                node->setFlag(QSGGeometryNode::OwnsMaterial);
                node->setGeometry(geometry);
                node->setFlag(QSGGeometryNode::OwnsGeometry);

                m_meshNode->appendChildNode(node);
            }
        }
    }

    // vertical lines
    if (m_maxTemp > m_minTemp)
    {
        const auto meshTemps = this->meshTemps();
        for (const int temp : meshTemps)
        {
            qreal x = tempToX(temp);

            if (x > rect.x() && x < rect.right())
            {
                auto material = new QSGFlatColorMaterial;
                material->setColor(m_meshColor);

                auto geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 2);
                geometry->setDrawingMode(QSGGeometry::DrawLines);
                geometry->setLineWidth(1);
                geometry->vertexDataAsPoint2D()[0].set(x, rect.top());
                geometry->vertexDataAsPoint2D()[1].set(x, rect.bottom());

                auto node = new QSGGeometryNode;
                node->setFlag(QSGNode::OwnedByParent);
                node->setMaterial(material);
                node->setFlag(QSGGeometryNode::OwnsMaterial);
                node->setGeometry(geometry);
                node->setFlag(QSGGeometryNode::OwnsGeometry);

                m_meshNode->appendChildNode(node);
            }
        }
    }

    m_meshChanged = false;
}

void FanGraph::updateCurveNode()
{
    if (!m_curveChanged)
        return;

    if (!m_fan || m_fan->controlMode() != PwmFan::CurveControl || m_fan->curve().isEmpty())
    {
        delete m_curveNode;
        m_curveNode = nullptr;

        return;
    }

    if (!m_curveNode)
        m_curveNode = new QSGGeometryNode;

    auto rect = borderedRect();
    auto vertices = QList<QPointF>();
    const auto curve = m_fan->curve();

    if (m_minTemp < curve.first().x())
    {
        vertices << QPointF(rect.left(), pwmToY(m_fan->minPwm()));
        vertices << QPointF(rect.left(), rect.bottom());
        vertices << QPointF(tempToX(curve.first().x()), pwmToY(m_fan->minPwm()));
    }

    for (const auto curvePoint : curve)
    {
        vertices << QPointF(tempToX(curvePoint.x()), rect.bottom());
        vertices << curvePointToCoordinates(curvePoint, true);
    }

    if (m_maxTemp > curve.last().x())
    {
        vertices << QPointF(rect.right(), rect.bottom());
        vertices << QPointF(rect.right(), pwmToY(curve.last().y(), true));
    }

    auto geometry = coloredGeometryFromVertices(vertices);
    geometry->setDrawingMode(QSGGeometry::DrawTriangleStrip);

    m_curveNode->setGeometry(geometry);
    m_curveNode->setFlag(QSGNode::OwnsGeometry);
    m_curveNode->setMaterial(new QSGVertexColorMaterial);
    m_curveNode->setFlag(QSGNode::OwnsMaterial);


    // Curve border
    auto borderNode = static_cast<QSGGeometryNode*>(m_curveNode->firstChild());

    if (!borderNode)
    {
        borderNode = new QSGGeometryNode;
        borderNode->setFlag(QSGNode::OwnedByParent);
        m_curveNode->appendChildNode(borderNode);
    }

    vertices.clear();

    if (m_minTemp < curve.first().x())
    {
        if (m_fan->minPwm() > 0)
        {
            vertices << QPointF(rect.left(), pwmToY(m_fan->minPwm()));
            vertices << QPointF(tempToX(curve.first().x()), pwmToY(m_fan->minPwm()));
        }
        else
            vertices << QPointF(tempToX(curve.first().x()), rect.bottom());
    }

    for (const auto curvePoint : curve)
    {
        vertices << curvePointToCoordinates(curvePoint, true);
    }

    if (curve.last().x() < m_maxTemp)
    {
        vertices << QPointF(rect.right(), pwmToY(curve.last().y(), true));
    }

    geometry = coloredGeometryFromVertices(vertices, false);
    geometry->setDrawingMode(QSGGeometry::DrawLineStrip);
    geometry->setLineWidth(3.0);

    borderNode->setGeometry(geometry);
    borderNode->setFlag(QSGNode::OwnsGeometry);
    borderNode->setMaterial(new QSGVertexColorMaterial);
    borderNode->setFlag(QSGNode::OwnsMaterial);

    m_curveChanged = false;
}

void FanGraph::updateAnchorNode()
{
    if (!m_fan || m_fan->controlMode() != PwmFan::AnchorControl || m_fan->anchor() < m_minTemp || m_fan->anchor() > m_maxTemp)
    {
        delete m_anchorNode;
        m_anchorNode = nullptr;

        return;
    }

    if (!m_anchorNode)
        m_anchorNode = new QSGGeometryNode;

    auto rect = borderedRect();
    auto x = tempToX(m_fan->anchor());

    auto geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 2);
    geometry->setDrawingMode(QSGGeometry::DrawLines);
    geometry->setLineWidth(2);
    geometry->vertexDataAsPoint2D()[0].set(x, rect.top());
    geometry->vertexDataAsPoint2D()[1].set(x, rect.bottom());

    auto material = new QSGFlatColorMaterial;
    material->setColor(m_anchorColor);

    m_anchorNode->setGeometry(geometry);
    m_anchorNode->setFlag(QSGGeometryNode::OwnsGeometry);
    m_anchorNode->setMaterial(material);
    m_anchorNode->setFlag(QSGGeometryNode::OwnsMaterial);
}

QList<int> FanGraph::meshTemps() const
{
    QList<int> temps;

    if (m_maxTemp < m_minTemp)
        return temps;

    if (m_minTemp == m_maxTemp)
        return temps << m_minTemp;

    for (int i = m_minTemp + 1; i < m_maxTemp; i++)
    {
        if (i % m_horMeshSpacing == 0)
            temps << i;
    }

    temps << m_minTemp << m_maxTemp;

    return temps;
}

QList<QPointF> FanGraph::curvePointPositions() const
{
    if (!m_fan || m_fan->controlMode() != PwmFan::CurveControl)
        return QList<QPointF>();

    QList<QPointF> points;

    const auto curve = m_fan->curve();
    for (const auto point : curve)
    {
        points << curvePointToCoordinates(point, true);
    }

    return points;
}

QPointF FanGraph::statusPointPosition() const
{
    if (!m_fan)
        return borderedRect().bottomLeft();

    return QPointF(tempToX(m_fan->temp()), pwmToY(m_fan->pwm()));
}

qreal FanGraph::tempToX(qreal temp) const
{
    auto rect = borderedRect();

    if (rect.width() <= 0.0)
        return qMin(width(), rect.left());

    qreal tempDelta = m_maxTemp - m_minTemp;

    if (tempDelta <= 0)
        return qMin(width(), rect.left());

    return rect.left() + (temp - m_minTemp) / tempDelta * rect.width();
}

qreal FanGraph::pwmToY(qreal pwm, bool considerMinPwm) const
{
    auto rect = borderedRect();

    if (rect.height() <= 0.0)
        return qMin(height(), rect.top());

    if (considerMinPwm && m_fan && pwm < m_fan->minPwm())
        pwm = m_fan->minPwm();

    return rect.bottom() - pwm / 255.0 * rect.height();
}

qreal FanGraph::xToTemp(qreal x) const
{
    auto rect = borderedRect();

    if (rect.width() <= 0.0)
        return m_minTemp;

    qreal tempDelta = m_maxTemp - m_minTemp;

    if (tempDelta <= 0)
        return m_minTemp;

    return (x - rect.x()) / rect.width() * tempDelta + m_minTemp;
}

qreal FanGraph::yToPwm(qreal y) const
{
    auto rect = borderedRect();

    if (rect.height() <= 0.0)
        return 255.0;

    return 255.0 - (y - rect.y()) / rect.height() * 255.0;
}

QPointF FanGraph::curvePointToCoordinates(QPoint point, bool considerMinPwm) const
{
    qreal x = tempToX(point.x());
    qreal y = pwmToY(point.y(), considerMinPwm);

    return QPointF(x, y);
}

QColor FanGraph::colorAt(QPointF point, bool calculateAlpha) const
{
    auto rect = borderedRect();

    if (rect.width() <= 0.0 || rect.height() <= 0.0)
        return Qt::black;

    int red = 255.0 * (point.x() - rect.x()) / rect.width();

    if (calculateAlpha)
    {
        int alpha = 25 + 150 * (rect.bottom() - point.y()) / rect.height();

        return QColor(red * alpha / 255, 0, (255 - red) * alpha / 255, alpha);
    }

    return QColor(red, 0, 255 - red);
}

QRectF FanGraph::borderedRect() const
{
    return QRectF(m_borderWidth, m_borderWidth, width() - 2 * m_borderWidth, height() - 2 * m_borderWidth);
}

QSGGeometry* FanGraph::coloredGeometryFromVertices(const QList<QPointF>& vertices, bool calculateAlpha)
{
    auto geometry = new QSGGeometry(QSGGeometry::defaultAttributes_ColoredPoint2D(), vertices.size());

    for (int i = 0; i < vertices.size(); i++)
    {
        auto vertex = vertices.at(i);
        auto color = colorAt(vertex, calculateAlpha);
        geometry->vertexDataAsColoredPoint2D()[i].set(vertex.x(), vertex.y(), color.red(), color.green(), color.blue(), color.alpha());
    }

    return geometry;
}

void FanGraph::setMinTemp(int minTemp)
{
    if (minTemp == m_minTemp)
        return;

    m_meshChanged = true;
    m_minTemp = minTemp;
    emit minTempChanged();
}

void FanGraph::setMaxTemp(int maxTemp)
{
    if (maxTemp == m_maxTemp)
        return;

    m_meshChanged = true;
    m_maxTemp = maxTemp;
    emit maxTempChanged();
}

void FanGraph::setHorMeshSpacing(int horMeshSpacing)
{
    if (horMeshSpacing < 1)
        horMeshSpacing = 1;

    if (horMeshSpacing == m_horMeshSpacing)
        return;

    m_meshChanged = true;
    m_horMeshSpacing = horMeshSpacing;
    emit horMeshSpacingChanged();
}

void FanGraph::setVerticalScalaCount(int verticalScalaCount)
{
    if (verticalScalaCount < 2)
        verticalScalaCount = 2;

    if (verticalScalaCount == m_verticalScalaCount)
        return;

    m_meshChanged = true;
    m_verticalScalaCount = verticalScalaCount;
    emit verticalScalaCountChanged();
}

void FanGraph::setBorderWidth(qreal borderWidth)
{
    if (borderWidth == m_borderWidth)
        return;

    m_borderWidth = borderWidth;
    emit borderWidthChanged();
}

void FanGraph::setBorderColor(const QColor& borderColor)
{
    if (borderColor == m_borderColor)
        return;

    m_borderColor = borderColor;
    emit borderColorChanged();
}

void FanGraph::setBackgroundColor(const QColor& backgroundColor)
{
    if (backgroundColor == m_backgroundColor)
        return;

    m_backgroundColor = backgroundColor;
    emit backgroundColorChanged();
}

void FanGraph::setMeshColor(const QColor& meshColor)
{
    if (meshColor == m_meshColor)
        return;

    m_meshChanged = true;
    m_meshColor = meshColor;
    emit meshColorChanged();
}

void FanGraph::setAnchorColor(const QColor& anchorColor)
{
    if (anchorColor == m_anchorColor)
        return;

    m_anchorColor = anchorColor;
    emit anchorColorChanged();
}

QObject* FanGraph::fan() const
{
    return static_cast<QObject*>(m_fan);
}


void FanGraph::setFan(QObject* fan)
{
    if (fan == m_fan)
        return;

    if (m_fan)
    {
        disconnect(m_fan, &PwmFan::anchorChanged, this, &QQuickItem::update);
        disconnect(m_fan, &PwmFan::controlModeChanged, this, &QQuickItem::update);
        disconnect(m_fan, &PwmFan::minPwmChanged, this, &FanGraph::updateCurve);

        disconnect(m_fan, &PwmFan::curveChanged, this, &FanGraph::curvePointPositionsChanged);
        disconnect(m_fan, &PwmFan::controlModeChanged, this, &FanGraph::curvePointPositionsChanged);
        disconnect(m_fan, &PwmFan::minPwmChanged, this, &FanGraph::curvePointPositionsChanged);

        disconnect(m_fan, &PwmFan::tempChanged, this, &FanGraph::statusPointPositionChanged);
        disconnect(m_fan, &PwmFan::pwmChanged, this, &FanGraph::statusPointPositionChanged);
    }

    m_fan = qobject_cast<PwmFan*>(fan);
    emit fanChanged();

    if (m_fan)
    {
        connect(m_fan, &PwmFan::anchorChanged, this, &QQuickItem::update);
        connect(m_fan, &PwmFan::controlModeChanged, this, &QQuickItem::update);
        connect(m_fan, &PwmFan::minPwmChanged, this, &FanGraph::updateCurve);

        connect(m_fan, &PwmFan::curveChanged, this, &FanGraph::curvePointPositionsChanged);
        connect(m_fan, &PwmFan::controlModeChanged, this, &FanGraph::curvePointPositionsChanged);
        connect(m_fan, &PwmFan::minPwmChanged, this, &FanGraph::curvePointPositionsChanged);

        connect(m_fan, &PwmFan::tempChanged, this, &FanGraph::statusPointPositionChanged);
        connect(m_fan, &PwmFan::pwmChanged, this, &FanGraph::statusPointPositionChanged);
    }
}

}

