/*
 * Copyright 2021  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FANGRAPH_H
#define FANGRAPH_H


#include <QtQuick/QQuickItem>


class QSGGeometry;
class QSGGeometryNode;

namespace Fancontrold
{

class PwmFan;

class FanGraph : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(int verticalScalaCount READ verticalScalaCount WRITE setVerticalScalaCount NOTIFY verticalScalaCountChanged)
    Q_PROPERTY(int minTemp READ minTemp WRITE setMinTemp NOTIFY minTempChanged)
    Q_PROPERTY(int maxTemp READ maxTemp WRITE setMaxTemp NOTIFY maxTempChanged)
    Q_PROPERTY(int horMeshSpacing READ horMeshSpacing WRITE setHorMeshSpacing NOTIFY horMeshSpacingChanged)
    Q_PROPERTY(qreal borderWidth READ borderWidth WRITE setBorderWidth NOTIFY borderWidthChanged)
    Q_PROPERTY(QColor borderColor READ borderColor WRITE setBorderColor NOTIFY borderColorChanged)
    Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor NOTIFY backgroundColorChanged)
    Q_PROPERTY(QColor meshColor READ meshColor WRITE setMeshColor NOTIFY meshColorChanged)
    Q_PROPERTY(QColor anchorColor READ anchorColor WRITE setAnchorColor NOTIFY anchorColorChanged)
    Q_PROPERTY(QObject* fan READ fan WRITE setFan NOTIFY fanChanged)
    Q_PROPERTY(QList<int> meshTemps READ meshTemps NOTIFY meshTempsChanged)
    Q_PROPERTY(QList<QPointF> curvePointPositions READ curvePointPositions NOTIFY curvePointPositionsChanged)
    Q_PROPERTY(QPointF statusPointPosition READ statusPointPosition NOTIFY statusPointPositionChanged)

public:

    FanGraph(QQuickItem *parent = nullptr);
    virtual ~FanGraph();

    int verticalScalaCount() const { return m_verticalScalaCount; }
    void setVerticalScalaCount(int verticalScalaCount);
    int minTemp() const { return m_minTemp; }
    void setMinTemp(int minTemp);
    int maxTemp() const { return m_maxTemp; }
    void setMaxTemp(int maxTemp);
    int horMeshSpacing() const { return m_horMeshSpacing; }
    void setHorMeshSpacing(int horMeshSpacing);
    qreal borderWidth() const { return m_borderWidth; }
    void setBorderWidth(qreal borderWidth);
    QColor borderColor() const { return m_borderColor; }
    void setBorderColor(const QColor &borderColor);
    QColor backgroundColor() const { return m_backgroundColor; }
    void setBackgroundColor(const QColor &backgroundColor);
    QColor meshColor() const { return m_meshColor; }
    void setMeshColor(const QColor &meshColor);
    QColor anchorColor() const { return m_anchorColor; }
    void setAnchorColor(const QColor &anchorColor);
    QObject* fan() const;
    void setFan(QObject *fan);
    QList<int> meshTemps() const;
    QList<QPointF> curvePointPositions() const;
    QPointF statusPointPosition() const;
    void updateMesh() { m_meshChanged = true; update(); }
    void updateCurve() { m_curveChanged = true; update(); }
    void updateBorder() { m_borderChanged = true; update(); }

    Q_INVOKABLE qreal tempToX(qreal temp) const;
    Q_INVOKABLE qreal pwmToY(qreal pwm, bool considerMinPwm = false) const;
    Q_INVOKABLE qreal xToTemp(qreal x) const;
    Q_INVOKABLE qreal yToPwm(qreal y) const;


protected:

    QSGNode* updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *data) override;
    void updateBackgroundNode();
    void updateMeshNode();
    void updateCurveNode();
    void updateAnchorNode();
    void updateBorderNode();
    QPointF curvePointToCoordinates(QPoint point, bool considerMinPwm = false) const;
    QColor colorAt(QPointF point, bool calculateAlpha = true) const;
    QRectF borderedRect() const;
    QSGGeometry* coloredGeometryFromVertices(const QList<QPointF> &vertices, bool calculateAlpha = true);

signals:

    void verticalScalaCountChanged();
    void minTempChanged();
    void maxTempChanged();
    void horMeshSpacingChanged();
    void borderWidthChanged();
    void borderColorChanged();
    void backgroundColorChanged();
    void meshColorChanged();
    void anchorColorChanged();
    void fanChanged();
    void meshTempsChanged();
    void curvePointPositionsChanged();
    void statusPointPositionChanged();


private:

    int m_verticalScalaCount;
    int m_minTemp;
    int m_maxTemp;
    int m_horMeshSpacing;
    qreal m_borderWidth;
    QColor m_borderColor;
    QColor m_backgroundColor;
    QColor m_meshColor;
    QColor m_anchorColor;
    PwmFan *m_fan;
    QSGGeometryNode *m_backgroundNode;
    QSGGeometryNode *m_borderNode;
    QSGGeometryNode *m_curveNode;
    QSGGeometryNode *m_anchorNode;
    QSGNode *m_meshNode;
    bool m_meshChanged;
    bool m_curveChanged;
    bool m_borderChanged;
};

}

#endif // FANGRAPH_H
