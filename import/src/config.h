/*
 * Copyright 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef CONFIG_H
#define CONFIG_H

#include <KConfigCore/KCoreConfigSkeleton>


namespace Fancontrold
{

class Config : public KCoreConfigSkeleton
{

    Q_OBJECT

    Q_PROPERTY(qreal minTemp READ minTemp WRITE setMinTemp NOTIFY minTempChanged)
    Q_PROPERTY(qreal maxTemp READ maxTemp WRITE setMaxTemp NOTIFY maxTempChanged)
    Q_PROPERTY(QString serviceName READ serviceName WRITE setServiceName NOTIFY serviceNameChanged)
    Q_PROPERTY(bool showScales READ showScales WRITE setShowScales NOTIFY showScalesChanged)
    Q_PROPERTY(bool showTray READ showTray WRITE setShowTray NOTIFY showTrayChanged)
    Q_PROPERTY(bool darkTray READ darkTray WRITE setDarkTray NOTIFY darkTrayChanged)
    Q_PROPERTY(bool startMinimized READ startMinimized WRITE setStartMinimized NOTIFY startMinimizedChanged)

public:

    static Config *instance();

    qreal minTemp() const;
    void setMinTemp(qreal minTemp);
    qreal maxTemp() const;
    void setMaxTemp(qreal maxTemp);
    QString serviceName() const;
    void setServiceName(const QString &name);
    bool showScales() const;
    void setShowScales(bool showScales);
    bool showTray() const;
    void setShowTray(bool showTray);
    bool darkTray() const;
    void setDarkTray(bool darkTray);
    bool startMinimized() const;
    void setStartMinimized(bool sm);

    QMap<QString, QString> profiles() const;
    void setProfiles(const QMap<QString, QString>& newProfiles);


signals:

    void minTempChanged();
    void maxTempChanged();
    void serviceNameChanged();
    void showScalesChanged();
    void showTrayChanged();
    void darkTrayChanged();
    void startMinimizedChanged();
    void profilesChanged();


private:

    Config(QObject *parent = nullptr);
    ~Config();
    Q_DISABLE_COPY(Config)

    static Config *m_instance;

    double m_minTemp;
    double m_maxTemp;
    QString m_serviceName;
    QStringList m_profiles;
    QStringList m_profileNames;
    int m_currentProfile;
    bool m_showScales;
    bool m_showTray;
    bool m_darkTray;
    bool m_startMinimized;
};

}

#endif // CONFIG_H
