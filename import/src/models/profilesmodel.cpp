/*
 * Copyright 2021  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "profilesmodel.h"

#include "../guibase.h"
#include "../loader.h"
#include "../config.h"

namespace Fancontrold
{

ProfilesModel::ProfilesModel(QObject *parent) : QAbstractListModel(parent)
{
    auto base = qobject_cast<GUIBase*>(parent);

    m_loader = base ? base->loader() : nullptr;

    connect(m_loader, &Loader::configChanged, this, &ProfilesModel::updateCurrent);

    auto config = Config::instance();
    setProfiles(config->profiles());
    connect(config, &Config::profilesChanged, this, [=] () { this->setProfiles(config->profiles()); });
}

QVariant ProfilesModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const int row = index.row();

    if (row >= rowCount())
        return QVariant();

    const auto profileName = m_profiles.keys().value(row);

    if (profileName.isEmpty())
        return QVariant();

    const auto profileConfig = m_profiles.value(profileName);

    switch (role)
    {
        case NameRole:
            return profileName;

        case ConfigRole:
            return profileConfig;

        case IndexRole:
            return row;

        case CurrentRole:
            if (!m_loader)
                return false;

            return profileConfig == m_loader->config();
    }

    return QVariant();
}

int ProfilesModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return m_profiles.size();
}

QHash<int, QByteArray> ProfilesModel::roleNames() const
{
    QHash<int, QByteArray> roleNames;

    roleNames.insert(NameRole, "name");
    roleNames.insert(ConfigRole, "config");
    roleNames.insert(IndexRole, "index");
    roleNames.insert(CurrentRole, "current");

    return roleNames;
}

void ProfilesModel::setProfiles(const QMap<QString, QString> &profiles)
{
    if (m_profiles == profiles)
        return;

    beginResetModel();

    m_profiles = profiles;

    endResetModel();
}

void ProfilesModel::updateCurrent()
{
    emit dataChanged(index(0), index(rowCount() - 1), QVector<int>{ CurrentRole });
}

}
