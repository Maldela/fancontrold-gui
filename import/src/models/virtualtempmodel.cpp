/*
 * Copyright 2023  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "virtualtempmodel.h"

#include "../hwmon.h"
#include "../sensors/virtualtemp.h"

#include <KLocalizedString>


#define UNIT_SUFFIX "°C"


namespace Fancontrold
{

VirtualTempModel::VirtualTempModel(QObject *parent) : QAbstractListModel(parent)
{
}

QHash<int, QByteArray> VirtualTempModel::roleNames() const
{
    QHash<int, QByteArray> roleNames;

    roleNames.insert(DisplayRole, "display");
    roleNames.insert(ValueRole, "value");
    roleNames.insert(PathRole, "path");
    roleNames.insert(ObjectRole, "object");
    roleNames.insert(HwmonRole, "hwmon");
    roleNames.insert(NameRole, "name");

    return roleNames;
}

QVariant VirtualTempModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const int row = index.row();

    if (row >= rowCount())
        return QVariant();

    const auto virtualTemp = m_virtualTemps.value(row);

    if (!virtualTemp)
        return QVariant();

    switch (role)
    {
        case DisplayRole:
            return virtualTemp->path() + ": " + QString::number(virtualTemp->value()) + UNIT_SUFFIX;

        case ValueRole:
            return virtualTemp->value();

        case PathRole:
            return virtualTemp->path();

        case ObjectRole:
            return QVariant::fromValue(static_cast<QObject*>(virtualTemp));

        case HwmonRole:
            return i18n("Virtual");

        case NameRole:
            return virtualTemp->path();

        default:
            return QVariant();
    }
}

void VirtualTempModel::setVirtualTemps(QList<VirtualTemp *> virtualTemps)
{
    std::sort(virtualTemps.begin(), virtualTemps.end(), [] (VirtualTemp *a, VirtualTemp *b) { return a->path() < b->path(); });

    if (m_virtualTemps == virtualTemps)
        return;

    beginResetModel();

    m_virtualTemps = virtualTemps;
    emit virtualTempsChanged();

    for (const auto &virtualTemp : std::as_const(virtualTemps))
    {
        connect(virtualTemp, &VirtualTemp::valueChanged, this, static_cast<void(VirtualTempModel::*)()>(&VirtualTempModel::updateValue));
    }

    endResetModel();
}

void VirtualTempModel::updateValue(VirtualTemp *virtualTemp)
{
    if (!virtualTemp)
        return;

    auto i = m_virtualTemps.indexOf(virtualTemp);

    if (i == -1)
        return;

    emit dataChanged(index(i, 0), index(i, 0), QVector<int>{ DisplayRole, ValueRole });
}

void VirtualTempModel::updateValue()
{
    const auto virtualTemp = qobject_cast<VirtualTemp *>(sender());

    updateValue(virtualTemp);
}

void VirtualTempModel::updateAll()
{
    emit dataChanged(index(0, 0), index(m_virtualTemps.size(), 0), QVector<int>{ DisplayRole, ValueRole, PathRole, ObjectRole, HwmonRole, NameRole });
}

}

