/*
 * Copyright 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef TEMPMODEL_H
#define TEMPMODEL_H


#include <QtCore/QAbstractListModel>


namespace Fancontrold
{

class Temp;

class TempModel : public QAbstractListModel
{
    Q_OBJECT

public:

    enum Roles
    {
        DisplayRole,
        ValueRole,
        PathRole,
        ObjectRole,
        HwmonRole,
        NameRole,
        LabelRole,
    };
    Q_ENUM(Roles)

    TempModel(QObject *parent = nullptr);
    void setTemps(QList<Temp *> temps);
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override { Q_UNUSED(parent) return m_temps.size(); }
    virtual QVariant data(const QModelIndex &index, int role = DisplayRole) const override;
    virtual QHash<int, QByteArray> roleNames() const override;
    void updateName(Temp *temp);
    void updateValue(Temp *temp);


protected:

    void updateAll();


signals:

    void tempsChanged();


private:

    void updateName();
    void updateValue();

    QList<Temp *> m_temps;
};

}


#endif //TEMPMODEL_H
