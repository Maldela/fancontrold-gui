/*
 * Copyright 2021  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROFILESMODEL_H
#define PROFILESMODEL_H

#include <QtCore/QAbstractListModel>

#include <QtCore/QMap>


namespace Fancontrold
{

class Loader;

class ProfilesModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles {
        NameRole,
        ConfigRole,
        IndexRole,
        CurrentRole
    };
    Q_ENUM(Roles)

    ProfilesModel(QObject *parent);

    QVariant data(const QModelIndex& index, int role) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QHash<int, QByteArray> roleNames() const override;

    void setProfiles(const QMap<QString, QString>& profiles);
    void updateCurrent();


private:

    Loader *m_loader;
    QMap<QString, QString> m_profiles;
};

}

#endif // PROFILESMODEL_H
