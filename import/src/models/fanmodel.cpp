/*
 * Copyright 2023  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "fanmodel.h"

#include "../hwmon.h"
#include "../sensors/fan.h"


#define UNIT_SUFFIX " rpm"


namespace Fancontrold
{

FanModel::FanModel(QObject *parent) : QAbstractListModel(parent)
{
}

QHash<int, QByteArray> FanModel::roleNames() const
{
    QHash<int, QByteArray> roleNames;

    roleNames.insert(DisplayRole, "display");
    roleNames.insert(ValueRole, "value");
    roleNames.insert(PathRole, "path");
    roleNames.insert(ObjectRole, "object");
    roleNames.insert(HwmonRole, "hwmon");
    roleNames.insert(NameRole, "name");

    return roleNames;
}

QVariant FanModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const int row = index.row();

    if (row >= rowCount())
        return QVariant();

    if (row < m_fans.size())
    {
        const auto fan = m_fans.at(row);

        if (!fan)
            return QVariant();

        switch (role)
        {
            case DisplayRole:
                return fan->name() + ": " + QString::number(fan->rpm()) + UNIT_SUFFIX;

            case ValueRole:
                return fan->rpm();

            case PathRole:
                return fan->path();

            case ObjectRole:
                return QVariant::fromValue(static_cast<QObject*>(fan));

            case HwmonRole:
                return fan->parent()->name();

            case NameRole:
                return QString("fan") + QString::number(fan->index());

            default:
                return QVariant();
        }
    }

    return QVariant();
}

void FanModel::setFans(QList<Fan *> fans)
{
    std::sort(fans.begin(), fans.end(), [] (Fan *a, Fan *b) { if (a->parent() == b->parent()) return a->index() < b->index(); return a->parent()->name() < b->parent()->name(); });

    if (m_fans == fans)
        return;

    beginResetModel();

    m_fans = fans;
    emit fansChanged();

    for (const auto &fan : std::as_const(fans))
    {
        connect(fan, &Fan::nameChanged, this, static_cast<void(FanModel::*)()>(&FanModel::updateName));
        connect(fan, &Fan::rpmChanged, this, static_cast<void(FanModel::*)()>(&FanModel::updateRpm));
    }

    endResetModel();
}

void FanModel::updateName(Fan *fan)
{
    if (!fan)
        return;

    const auto i = m_fans.indexOf(fan);

    if (i == -1)
        return;

    emit dataChanged(index(i, 0), index(i, 0), QVector<int>{ DisplayRole, NameRole });
}

void FanModel::updateRpm(Fancontrold::Fan* fan)
{
    if (!fan)
        return;

    const auto i = m_fans.indexOf(fan);

    if (i == -1)
        return;

    emit dataChanged(index(i, 0), index(i, 0), QVector<int>{ DisplayRole, ValueRole });
}

void FanModel::updateName()
{
    const auto fan = qobject_cast<Fan *>(sender());

    updateName(fan);
}

void FanModel::updateRpm()
{
    const auto fan = qobject_cast<Fan *>(sender());

    updateRpm(fan);
}

void FanModel::updateAll()
{
    emit dataChanged(index(0, 0), index(m_fans.size(), 0), QVector<int>{ DisplayRole, ValueRole, PathRole, ObjectRole, HwmonRole, NameRole });
}

}

