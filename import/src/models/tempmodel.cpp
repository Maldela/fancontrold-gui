/*
 * Copyright 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "tempmodel.h"

#include "../hwmon.h"
#include "../sensors/temp.h"

#include <KLocalizedString>


#define UNIT_SUFFIX "°C"


namespace Fancontrold
{

TempModel::TempModel(QObject *parent) : QAbstractListModel(parent)
{
}

QHash<int, QByteArray> TempModel::roleNames() const
{
    QHash<int, QByteArray> roleNames;

    roleNames.insert(DisplayRole, "display");
    roleNames.insert(ValueRole, "value");
    roleNames.insert(PathRole, "path");
    roleNames.insert(ObjectRole, "object");
    roleNames.insert(HwmonRole, "hwmon");
    roleNames.insert(NameRole, "name");
    roleNames.insert(LabelRole, "label");

    return roleNames;
}

QVariant TempModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const int row = index.row();

    if (row >= rowCount())
        return QVariant();

    const auto temp = m_temps.value(row);

    if (!temp)
        return QVariant();

    switch (role)
    {
        case DisplayRole:
            return temp->label() + ": " + QString::number(temp->value()) + UNIT_SUFFIX;

        case ValueRole:
            return temp->value();

        case PathRole:
            return temp->path();

        case ObjectRole:
            return QVariant::fromValue(static_cast<QObject*>(temp));

        case HwmonRole:
            return temp->parent()->name();

        case NameRole:
            return temp->name();

        case LabelRole:
            return temp->label();

        default:
            return QVariant();
    }
}

void TempModel::setTemps(QList<Temp *> temps)
{
    std::sort(temps.begin(), temps.end(), [] (Temp *a, Temp *b) { if (a->parent() == b->parent()) return a->index() < b->index(); return a->parent()->name() < b->parent()->name(); });

    if (m_temps == temps)
        return;

    beginResetModel();

    m_temps = temps;
    emit tempsChanged();

    for (const auto &temp : std::as_const(temps))
    {
        connect(temp, &Temp::nameChanged, this, static_cast<void(TempModel::*)()>(&TempModel::updateName));
        connect(temp, &Temp::valueChanged, this, static_cast<void(TempModel::*)()>(&TempModel::updateValue));
    }

    endResetModel();
}

void TempModel::updateName(Temp *temp)
{
    if (!temp)
        return;

    const auto i = m_temps.indexOf(temp);

    if (i == -1)
        return;

    emit dataChanged(index(i, 0), index(i, 0), QVector<int>{ DisplayRole, NameRole });
}

void TempModel::updateValue(Temp *temp)
{
    if (!temp)
        return;

    const auto i = m_temps.indexOf(temp);

    if (i == -1)
        return;

    emit dataChanged(index(i, 0), index(i, 0), QVector<int>{ DisplayRole, ValueRole });
}

void TempModel::updateName()
{
    const auto temp = qobject_cast<Temp *>(sender());

    updateName(temp);
}

void TempModel::updateValue()
{
    const auto temp = qobject_cast<Temp *>(sender());

    updateValue(temp);
}

void TempModel::updateAll()
{
    emit dataChanged(index(0, 0), index(m_temps.size(), 0), QVector<int>{ DisplayRole, ValueRole, PathRole, ObjectRole, HwmonRole, NameRole, LabelRole });
}

}

