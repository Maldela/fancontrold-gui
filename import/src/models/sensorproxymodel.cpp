/*
 * Copyright 2023  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sensorproxymodel.h"

#include "fanmodel.h"
#include "tempmodel.h"
#include "virtualtempmodel.h"
#include "../sensors/sensor.h"
#include "../hwmon.h"

#include <QDebug>

namespace Fancontrold
{

SensorProxyModel::SensorProxyModel::SensorProxyModel(QObject* parent) : QConcatenateTablesProxyModel(parent)
{
}

QHash< int, QByteArray > SensorProxyModel::roleNames() const
{
    QHash<int, QByteArray> roleNames;

    roleNames.insert(DisplayRole, "display");
    roleNames.insert(ValueRole, "value");
    roleNames.insert(PathRole, "path");
    roleNames.insert(ObjectRole, "object");
    roleNames.insert(HwmonRole, "hwmon");
    roleNames.insert(NameRole, "name");
    roleNames.insert(LabelRole, "label");
    roleNames.insert(TypeRole, "type");

    return roleNames;
}

QVariant SensorProxyModel::data(const QModelIndex& index, int role) const
{
    if (role == Roles::TypeRole)
    {
        auto sourceIndex = mapToSource(index);
        auto sourceModel = sourceIndex.model();

        if (qobject_cast<const FanModel*>(sourceModel))
            return SensorType::Fan;

        if (qobject_cast<const TempModel*>(sourceModel))
            return SensorType::Temp;

        if (qobject_cast<const VirtualTempModel*>(sourceModel))
            return SensorType::VirtualTemp;
    }

    return QConcatenateTablesProxyModel::data(index, role);
}


SensorSortProxyModel::SensorSortProxyModel(QObject* parent, ModelType type) : QSortFilterProxyModel(parent),
    m_sensorProxyModel(new SensorProxyModel(this)),
    m_type(type)
{
    setDynamicSortFilter(true);
    setSourceModel(m_sensorProxyModel);
}

bool SensorSortProxyModel::lessThan(const QModelIndex& source_left, const QModelIndex& source_right) const
{
    auto leftType = source_left.data(SensorProxyModel::Roles::TypeRole).toInt();
    auto rightType = source_right.data(SensorProxyModel::Roles::TypeRole).toInt();

    if (leftType == SensorProxyModel::SensorType::VirtualTemp && rightType == SensorProxyModel::SensorType::VirtualTemp)
        return source_left.data(SensorProxyModel::Roles::PathRole).toString() < source_right.data(SensorProxyModel::Roles::PathRole).toString();

    if (rightType == SensorProxyModel::SensorType::VirtualTemp)
        return true;

    if (leftType == SensorProxyModel::SensorType::VirtualTemp)
        return false;

    auto leftSensor = qobject_cast<Sensor*>(qvariant_cast<QObject*>(source_left.data(SensorProxyModel::Roles::ObjectRole)));
    auto rightSensor = qobject_cast<Sensor*>(qvariant_cast<QObject*>(source_right.data(SensorProxyModel::Roles::ObjectRole)));

    if (!leftSensor || !rightSensor)
        return false;

    if (leftSensor->parent()->index() < rightSensor->parent()->index())
        return true;

    if (leftSensor->path() < rightSensor->path())
        return true;

    return false;
}

bool SensorSortProxyModel::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const
{
    switch (m_type)
    {
        case ModelType::All:
            return true;

        case ModelType::CombinedTemp:
            auto role = m_sensorProxyModel->index(source_row, 0, source_parent).data(SensorProxyModel::Roles::TypeRole);
            if (role == SensorProxyModel::SensorType::Temp || role == SensorProxyModel::SensorType::VirtualTemp)
                return true;
            else
                return false;
    }

    return true;
}

void SensorSortProxyModel::addSourceModel(QAbstractItemModel* model)
{
    m_sensorProxyModel->addSourceModel(model);
    sort(0);
}


}
