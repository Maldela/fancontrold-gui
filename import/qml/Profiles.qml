/*
 * Copyright (C) 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.3 as Kirigami
import Fancontrold.Qml 1.0 as Fancontrold

ListView {
    id: profilesListView

    Layout.fillWidth: true
    implicitWidth: Kirigami.Units.gridUnit * 16

    model: Fancontrold.Base.profilesModel
    boundsBehavior: Flickable.StopAtBounds
    flickableDirection: Flickable.AutoFlickIfNeeded

    delegate: Kirigami.BasicListItem {
        id: listItem

        label: name
        subtitle: current ? i18n("(current profile)") : ""
        reserveSpaceForIcon: false
        hoverEnabled: true
        supportsMouseEvents: false
        separatorVisible: false

        RowLayout {
            Layout.alignment: Qt.AlignRight

            visible: listItem.hovered

            Button {
                ToolTip.text: i18n("Apply profile")
                ToolTip.visible: hovered
                ToolTip.delay: 1000

                icon.name: "dialog-ok-apply"
                enabled: !current
                onClicked: Fancontrold.Base.applyProfile(name)
            }
            Button {
                ToolTip.text: i18n("Overwrite profile")
                ToolTip.visible: hovered
                ToolTip.delay: 1000

                icon.name: "document-save"
                enabled: !current
                onClicked: Fancontrold.Base.saveProfile(name)
            }
            Button {
                ToolTip.text: i18n("Delete profile")
                ToolTip.visible: hovered
                ToolTip.delay: 1000

                icon.name: "edit-delete"
                onClicked: Fancontrold.Base.deleteProfile(name)
            }
        }
    }

    footerPositioning: ListView.OverlayFooter

    footer: Kirigami.AbstractListItem {
        id: footer

        background: Item {}

        RowLayout {
            width: footer.width

            Button {
                id: newProfileButton

                Layout.alignment: Qt.AlignRight

                icon.name: "contact-new"
                text: i18n("Create new profile")
                onClicked: {
                    newProfileNameField.open();
                    visible = false;
                }
            }

            TextField {
                id: newProfileNameField

                visible: false
                Layout.fillWidth: true

                placeholderText: i18n("New profile name")

                function open() {
                    visible = true;
                    focus = true;
                }

                onAccepted: {
                    Fancontrold.Base.saveProfile(newProfileNameField.text);
                    newProfileNameField.text = "";
                    newProfileButton.visible = true;
                    visible = false;
                }
            }

            Button {
                visible: newProfileNameField.visible

                ToolTip.text: i18n("Create new profile")
                ToolTip.visible: hovered
                ToolTip.delay: 1000

                icon.name: "dialog-ok-apply"
                onClicked: {
                    Fancontrold.Base.saveProfile(newProfileNameField.text);
                    newProfileNameField.text = "";
                    newProfileButton.visible = true;
                    visible = false;
                }
            }

            Button {
                visible: newProfileNameField.visible

                ToolTip.text: i18n("Cancel")
                ToolTip.visible: hovered
                ToolTip.delay: 1000

                icon.name: "dialog-cancel"
                onClicked: {
                    newProfileButton.visible = true;
                    newProfileNameField.text = "";
                    newProfileNameField.visible = false;
                }
            }
        }
    }
}
