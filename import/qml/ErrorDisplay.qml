/*
 * Copyright (C) 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.19 as Kirigami
import Fancontrold.Qml 1.0 as Fancontrold


Item {
    id: root

    property int maximumNotificationCount: 4

    function showNotification(message, timeout, type) {
        if (!message) {
            return;
        }

        let interval = 7000;

        if (timeout === "short") {
            interval = 4000;
        } else if (timeout === "long") {
            interval = 12000;
        } else if (timeout > 0) {
            interval = timeout;
        }

        let t = Kirigami.MessageType.Error;

        if (type == Kirigami.MessageType.Warning)
        {
            t = Kirigami.MessageType.Warning;
        }

        notificationsModel.append({
            text: message,
            type: t,
            closeInterval: interval,
        })

        // remove the oldest notification if new notification count would exceed 3
        if (notificationsModel.count === maximumNotificationCount) {
            if (listView.itemAtIndex(0).hovered === true) {
                hideNotification(1)
            } else {
                hideNotification()
            }
        }
    }

    function hideNotification(index = 0) {
        if (index >= 0 && notificationsModel.count > index) {
            notificationsModel.remove(index)
        }
    }

    // we have to set height to show more than one notification
    height: Math.min(applicationWindow().height, Kirigami.Units.gridUnit * 10)

    z: 10

    implicitHeight: listView.implicitHeight
    implicitWidth: listView.implicitWidth

    Kirigami.Theme.inherit: false
    Kirigami.Theme.colorSet: Kirigami.Theme.Complementary

    ListModel {
        id: notificationsModel
    }

    ListView {
        id: listView

        anchors.fill: parent
        anchors.bottomMargin: Kirigami.Units.largeSpacing
        anchors.topMargin: Kirigami.Units.largeSpacing

        spacing: Kirigami.Units.smallSpacing
        model: notificationsModel
        verticalLayoutDirection: ListView.TopToBottom
        focus: false
        interactive: false

        add: Transition {
            id: addAnimation
            ParallelAnimation {
                alwaysRunToEnd: true
                NumberAnimation {
                    property: "opacity"
                    from: 0
                    to: 1
                    duration: Kirigami.Units.longDuration
                    easing.type: Easing.OutCubic
                }
                NumberAnimation {
                    property: "y"
                    from: addAnimation.ViewTransition.destination.y + Kirigami.Units.gridUnit * 3
                    duration: Kirigami.Units.longDuration
                    easing.type: Easing.OutCubic
                }
            }
        }
        displaced: Transition {
            ParallelAnimation {
                alwaysRunToEnd: true
                NumberAnimation {
                    property: "y"
                    duration: Kirigami.Units.longDuration
                    easing.type: Easing.InOutCubic
                }
                NumberAnimation {
                    property: "opacity"
                    duration: 0
                    to: 1
                }
            }
        }
        remove: Transition {
            ParallelAnimation {
                alwaysRunToEnd: true
                NumberAnimation {
                    property: "opacity"
                    from: 1
                    to: 0
                    duration: Kirigami.Units.longDuration
                    easing.type: Easing.InCubic
                }
                NumberAnimation {
                    property: "y"
                    to: Kirigami.Units.gridUnit * 3
                    duration: Kirigami.Units.longDuration
                    easing.type: Easing.InCubic
                }
                PropertyAction {
                    property: "transformOrigin"
                    value: Item.Bottom
                }
                PropertyAnimation {
                    property: "scale"
                    from: 1
                    to: 0
                    duration: Kirigami.Units.longDuration
                    easing.type: Easing.InCubic
                }
            }
        }
        delegate: Kirigami.InlineMessage {
            id: delegate

            text: model.text
            type: model.type
            showCloseButton: true
            width: root.width
            visible: true

            Timer {
                id: timer
                interval: model.closeInterval
                running: !delegate.hovered
                onTriggered: hideNotification(index)
            }
        }
    }

    Connections {
        target: Fancontrold.Base

        function onError(e) { root.showNotification(e, 99999, Kirigami.MessageType.Error); }
        function onWarning(w) { root.showNotification(w, 7000, Kirigami.MessageType.Warning); }
    }
}
