/*
 * Copyright (C) 2019  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.2
import Fancontrold.Qml 1.0 as Fancontrold


Column {
    id: root

    property int padding
    required property QtObject fan
    readonly property QtObject tempModel: Fancontrold.Base.tempModel
    readonly property bool daemonConnected: Fancontrold.Base.loader.daemonConnected

    signal tempDrawerRequested()

    spacing: 2

    RowLayout {
        id: controlModeField

        enabled: !!fan ? daemonConnected : false
        width: parent.width

        Label {
            text: i18n("Control mode:")
            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
            renderType: Text.NativeRendering
        }

        ComboBox {
            id: controlModeInput

            Layout.fillWidth: true
            Layout.minimumWidth: implicitWidth / 2
            model: ["Curve", "Anchor", "Disabled"]
            currentIndex: !!fan ? fan.controlMode : 2
            onCurrentIndexChanged: {
                if (!!fan && fan.controlMode != currentIndex) {
                    fan.controlMode = currentIndex;
                }
            }

            Connections {
                target: root
                function onFanChanged() { if (!!fan) controlModeInput.currentIndex = fan.controlMode; }
            }
            Connections {
                target: fan
                function onControlModeChanged() { controlModeInput.currentIndex = fan.controlMode; }
            }
        }

        Label {
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            visible: !!fan ? fan.controlMode !== Fancontrold.PwmFan.NoControl && fan.temps.length === 0 : false
            horizontalAlignment: Text.AlignRight
            text: i18n("No temps selected for this fan. Please select temps here:")
            color: "red"
        }

        Button {
            id: tempButton

            Layout.alignment: Qt.AlignRight
            text: i18n("Choose temps")
            enabled: !!fan ? fan.controlMode !== Fancontrold.PwmFan.NoControl : false

            onClicked: root.tempDrawerRequested()
        }
    }

    RowLayout {
        enabled: !!fan ? fan.controlMode !== Fancontrold.PwmFan.NoControl && daemonConnected : false
        width: parent.width

        Label {
            text: i18n("Number of cycles to average temperature:")
            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
            renderType: Text.NativeRendering
        }
        SpinBox {
            id: averageInput

            Layout.fillWidth: true
            from: 1
            to: 100
            editable: true
            value: !!fan ? fan.average : 1
            textFromValue: function(value, locale) { return Number(value).toLocaleString(locale, 'f', 0) }
            onValueModified: {
                if (!!fan) {
                    fan.average = value
                }
            }

            Connections {
                target: root
                function onFanChanged() { if (!!fan) averageInput.value = fan.average; }
            }
            Connections {
                target: fan
                function onAverageChanged() { averageInput.value = fan.average; }
            }
        }
    }

    RowLayout {
        enabled: !!fan ? fan.controlMode !== Fancontrold.PwmFan.NoControl && daemonConnected : false
        width: parent.width

        Label {
            text: i18n("Minimum pwm value for fan to start:")
            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
            renderType: Text.NativeRendering
        }
        SpinBox {
            id: minStartInput

            Layout.fillWidth: true
            from: 0
            to: 100
            editable: true
            inputMethodHints: Qt.ImhDigitsOnly
            value: !!fan ? Math.round(fan.minStart / 2.55) : 0
            textFromValue: function(value, locale) { return Number(value).toLocaleString(locale, 'f', 0) + locale.percent }
            valueFromText: function(text, locale) { return text.split('%')[0]; }
            onValueModified: {
                if (!!fan && fan.minStart != Math.round(value * 2.55)) {
                    fan.minStart = Math.round(value * 2.55)
                }
            }

            Connections {
                target: root
                function onFanChanged() { if (!!fan) minStartInput.value = Math.round(fan.minStart / 2.55) }
            }
            Connections {
                target: fan
                function onMinStartChanged() { minStartInput.value = Math.round(fan.minStart / 2.55); }
            }
        }
    }

    RowLayout {
        enabled: !!fan ? fan.controlMode !== Fancontrold.PwmFan.NoControl && daemonConnected : false
        width: parent.width

        Label {
            text: i18n("Minimum pwm value:")
            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
            renderType: Text.NativeRendering
        }
        SpinBox {
            id: minPwmInput

            Layout.fillWidth: true
            from: 0
            to: 100
            editable: true
            inputMethodHints: Qt.ImhDigitsOnly
            value: !!fan ? Math.round(fan.minPwm / 2.55) : 0
            textFromValue: function(value, locale) { return Number(value).toLocaleString(locale, 'f', 0) + locale.percent }
            valueFromText: function(text, locale) { return text.split('%')[0]; }
            onValueModified: {
                if (!!fan && fan.minPwm != Math.round(value * 2.55)) {
                    fan.minPwm = Math.round(value * 2.55)
                }
            }

            Connections {
                target: root
                function onFanChanged() { if (!!fan) minPwmInput.value = Math.round(fan.minPwm / 2.55); }
            }
            Connections {
                target: fan
                function onMinPwmChanged() { minPwmInput.value = Math.round(fan.minPwm / 2.55); }
            }
        }
    }

    RowLayout {
        enabled: !!fan ? fan.controlMode !== Fancontrold.PwmFan.NoControl && daemonConnected : false
        width: parent.width

        Label {
            text: i18n("Pwm mode:")
            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
            renderType: Text.NativeRendering
        }
        ComboBox {
            id: pwmModeInput

            Layout.fillWidth: true
            model: ["DC", "PWM", "Automatic"]
            currentIndex: !!fan ? fan.pwmMode : 2
            onCurrentIndexChanged: {
                if (!!fan && fan.pwmMode != currentIndex) {
                    fan.pwmMode = currentIndex;
                }
            }

            Connections {
                target: root
                function onFanChanged() { if (!!fan) pwmModeInput.currentIndex = fan.pwmMode; }
            }
            Connections {
                target: fan
                function onPwmModeChanged() { pwmModeInput.currentIndex = fan.pwmMode; }
            }
        }
    }
}
