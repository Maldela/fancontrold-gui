/*
 * Copyright (C) 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.1
import org.kde.kirigami 2.3 as Kirigami
import Fancontrold.Qml 1.0 as Fancontrold


Kirigami.FormLayout {
    id: root

    readonly property QtObject systemdCom: Fancontrold.Base.hasSystemdCommunicator ? Fancontrold.Base.systemdCom : null
    readonly property QtObject loader: Fancontrold.Base.loader
    property bool showAll: true

    SpinBox {
        id: minTempBox

        readonly property string suffix: i18n("°C")

        Kirigami.FormData.label: i18n("Minimum temperature for fan graphs:")

        Layout.fillWidth: true
        from: -273
        to: 999
        inputMethodHints: Qt.ImhFormattedNumbersOnly
        editable: true
        value: Fancontrold.Config.minTemp
        textFromValue: function(value, locale) { return Number(value).toLocaleString(locale, 'f', 2) + suffix }
        valueFromText: function(text, locale) { return Number.fromLocaleString(locale, text.replace(suffix, "")); }

        onValueChanged: {
            if (value >= maxTempBox.value)
                maxTempBox.value = value + 1;
            Fancontrold.Config.minTemp = value;
        }

        Connections {
            target: Fancontrold.Config
            function onMinTempChanged() {
                if (Fancontrold.Config.minTemp !== minTempBox.value)
                    minTempBox.value = Fancontrold.Config.minTemp;
            }
        }
    }
    SpinBox {
        id: maxTempBox

        readonly property string suffix: i18n("°C")

        Kirigami.FormData.label: i18n("Maximum temperature for fan graphs:")

        Layout.fillWidth: true
        from: -273
        to: 999
        inputMethodHints: Qt.ImhFormattedNumbersOnly
        editable: true
        value: Fancontrold.Config.maxTemp
        textFromValue: function(value, locale) { return Number(value).toLocaleString(locale, 'f', 2) + suffix }
        valueFromText: function(text, locale) { return Number.fromLocaleString(locale, text.replace(suffix, "")); }

        onValueChanged: {
            if (value <= minTempBox.value)
                minTempBox.value = value - 1;
            Fancontrold.Config.maxTemp = value;
        }

        Connections {
            target: Fancontrold.Config
            function onMaxTempChanged() {
                if (Fancontrold.Config.maxTemp !== maxTempBox.celsuisValue)
                    maxTempBox.value = Fancontrold.Config.maxTemp;
            }
        }
    }
    TextField {
        id: serviceNameInput

        Kirigami.FormData.label: i18n("Name of the fancontrold systemd service:")

        visible: !!systemdCom

        Layout.fillWidth: true
        color: !!systemdCom && systemdCom.serviceExists ? "green" : "red"
        text: Fancontrold.Config.serviceName
        onTextChanged: Fancontrold.Config.serviceName = text

        Connections {
            target: Fancontrold.Config
            function onServiceNameChanged() { if(Fancontrold.Config.serviceName != serviceNameInput.text) serviceNameInput.text = Fancontrold.Config.serviceName; }
        }
    }
    CheckBox {
        id: showScalesBox

        Kirigami.FormData.label: i18n("Show scales on fan graphs:")

        visible: showAll

        checked: Fancontrold.Config.showScales
        onCheckedChanged: Fancontrold.Config.showScales = checked

        Connections {
            target: Fancontrold.Config
            function onShowScalesChanged() { if (Fancontrold.Config.showScales != showScalesBox.checked) showScalesBox.checked = Fancontrold.Config.showScales; }
        }
    }
    CheckBox {
        id: trayBox

        Kirigami.FormData.label: i18n("Show tray icon:")

        checked: Fancontrold.Config.showTray
        visible: showAll
        onCheckedChanged: Fancontrold.Config.showTray = checked

        Connections {
            target: Fancontrold.Config
            function onShowTrayChanged() { if (Fancontrold.Config.showTray != trayBox.checked) trayBox.checked = Fancontrold.Config.showTray; }
        }
    }
    CheckBox {
        id: darkTrayBox

        Kirigami.FormData.label: i18n("Show dark tray icon:")

        checked: Fancontrold.Config.darkTray
        visible: showAll && trayBox.checked
        onCheckedChanged: Fancontrold.Config.darkTray = checked

        Connections {
            target: Fancontrold.Config
            function onDarkTrayChanged() { if (Fancontrold.Config.darkTray != darkTrayBox.checked) darkTrayBox.checked = Fancontrold.Config.darkTray; }
        }
    }
    CheckBox {
        id: startMinimizedBox

        Kirigami.FormData.label: i18n("Start minimized:")
        checked: Fancontrold.Config.startMinimized
        visible: showAll && trayBox.checked
        onCheckedChanged: Fancontrold.Config.startMinimized = checked

        Connections {
            target: Fancontrold.Config
            function onStartMinimizedChanged() { if (Fancontrold.Config.startMinimized != startMinimizedBox.checked) startMinimizedBox.checked = Fancontrold.Config.startMinimized; }
        }
    }
}
