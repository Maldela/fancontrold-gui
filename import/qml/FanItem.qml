/*
 * Copyright (C) 2015  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.3 as Kirigami
import Fancontrold.Qml 1.0 as Fancontrold
import "math.js" as MoreMath


Item {
    id: root

    required property QtObject fan
    property int margin: Kirigami.Units.smallSpacing
    property bool showControls: true
    property bool editable: true
    property bool showScales: Fancontrold.Config.showScales
    readonly property QtObject tempModel: Fancontrold.Base.tempModel
    readonly property QtObject loader: Fancontrold.Base.loader
    readonly property bool daemonConnected: loader.daemonConnected
    readonly property real minTemp: Fancontrold.Config.minTemp
    readonly property real maxTemp: Fancontrold.Config.maxTemp
    readonly property real tempRange: Math.max(0, maxTemp - minTemp)

    signal tempDrawerRequested()

    Item {
        id: graph

        property int fontSize: MoreMath.bound(8, height / 20 + 1, 16)
        property int verticalScalaCount: height > Kirigami.Units.gridUnit * 30 ? 11 : 6

        visible: graphBackground.height > 0 && graphBackground.width > 0

        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
            bottom: root.showControls ? settingsArea.top : parent.bottom
            bottomMargin: root.margin
        }

        Item {
            id: verticalScala

            visible: root.showScales

            anchors {
                left: parent.left
                top: graphBackground.top
                bottom: graphBackground.bottom
            }
            width: MoreMath.maxWidth(children) + graph.fontSize / 3

            Repeater {
                id: verticalRepeater

                model: graph.verticalScalaCount

                Text {
                    x: verticalScala.width - implicitWidth - graph.fontSize / 3
                    y: graphBackground.height - graphBackground.height / (graph.verticalScalaCount - 1) * index - graph.fontSize * 2 / 3
                    horizontalAlignment: Text.AlignRight
                    color: Kirigami.Theme.textColor
                    text: Number(index * (100 / (graph.verticalScalaCount - 1))).toLocaleString(Qt.locale(), 'f', 0) + Qt.locale().percent
                    font.pixelSize: graph.fontSize
                }
            }
        }

        Item {
            id: horizontalScala

            visible: root.showScales

            anchors {
                left: graphBackground.left
                right: graphBackground.right
                bottom: parent.bottom
            }
            height: graph.fontSize * 2

            Repeater {
                model: graphBackground.meshTemps.length;

                Text {
                    x: graphBackground.width * (graphBackground.meshTemps[index] - graphBackground.minTemp) / (graphBackground.maxTemp - graphBackground.minTemp) - width / 2
                    y: horizontalScala.height / 2 - implicitHeight / 2
                    color: Kirigami.Theme.textColor
                    text: Number(graphBackground.meshTemps[index]).toLocaleString() + i18n("°C")
                    font.pixelSize: graph.fontSize
                }
            }
        }

        Fancontrold.FanGraph {
            id: graphBackground

            Kirigami.Theme.inherit: false
            Kirigami.Theme.colorSet: Kirigami.Theme.View

            backgroundColor: Kirigami.Theme.backgroundColor
            verticalScalaCount: graph.verticalScalaCount
            minTemp: root.minTemp
            maxTemp: root.maxTemp
            fan: root.fan

            anchors {
                top: parent.top
                left: root.showScales ? verticalScala.right : parent.left
                bottom: root.showScales ? horizontalScala.top : parent.bottom
                right: parent.right
                topMargin: root.showScales ? parent.fontSize : 0
                rightMargin: root.showScales ? parent.fontSize * 2 : Kirigami.Units.smallSpacing
                leftMargin: root.showScales ? 0 : Kirigami.Units.smallSpacing
            }

            MouseArea {
                anchors.fill: parent
                enabled: !!fan && fan.controlMode !== Fancontrold.PwmFan.NoControl && root.editable && daemonConnected

                onClicked: {
                    if (fan.controlMode === Fancontrold.PwmFan.CurveControl) {
                        var curve = fan.curve;
                        var temp = Math.round(graphBackground.xToTemp(mouse.x));
                        var pwm = Math.max(fan.minPwm, Math.round(graphBackground.yToPwm(mouse.y)));
                        var length = curve.length;
                        for (var i=0; i<length; i++) {
                            if(curve[i].x == temp) {
                                curve[i].y = pwm;
                                fan.curve = curve;
                                return;
                            }
                        }
                        curve[length] = Qt.point(temp, pwm);
                        fan.curve = curve;
                    } else if (fan.controlMode === Fancontrold.PwmFan.AnchorControl) {
                        fan.anchor = graphBackground.xToTemp(mouse.x);
                    }
                }
            }

            Repeater {
                id: pointsRepeater

                model: graph.visible ? graphBackground.curvePointPositions.length : 0

                PwmPoint {
                    id: point

                    property PwmPoint leftNeighbour: index > 0 ? pointsRepeater.itemAt(index - 1) : null
                    property PwmPoint rightNeighbour: index < pointsRepeater.count - 1 ? pointsRepeater.itemAt(index + 1) : null

                    draggable: root.editable
                    size: graph.fontSize
                    minimumX: !!leftNeighbour ? Math.max(graphBackground.tempToX(graphBackground.xToTemp(leftNeighbour.x) + 1), leftNeighbour.x + 1) : - size / 2
                    maximumX: !!rightNeighbour ? Math.min(graphBackground.tempToX(graphBackground.xToTemp(rightNeighbour.x) - 1), rightNeighbour.x - 1) : graphBackground.width - size / 2
                    maximumY: Math.min(graphBackground.pwmToY(fan.minPwm) - size / 2, graphBackground.height - size / 2)
                    x: graphBackground.curvePointPositions[index].x - width / 2
                    y: graphBackground.curvePointPositions[index].y - height / 2
                    visible: !!root.fan && root.fan.controlMode === Fancontrold.PwmFan.CurveControl

                    onDragged: {
                        var curve = root.fan.curve;
                        curve[index].y = Math.round(graphBackground.yToPwm(centerY));
                        curve[index].x = Math.round(graphBackground.xToTemp(centerX));
                        root.fan.curve = curve;
                    }
                    onRemove: {
                        var curve = fan.curve;
                        curve.splice(index, 1);
                        fan.curve = curve;
                    }
                }
            }

            Label {
                anchors.centerIn: parent
                color: "red"
                horizontalAlignment: Text.AlignHCenter
                visible: !!fan ? fan.curve.length === 0 && fan.controlMode === Fancontrold.PwmFan.CurveControl : false
                text: i18n("Fan has empty curve.\nPlease add points to the curve by clicking on the graph.")
            }

            Label {
                anchors.centerIn: parent
                color: "red"
                horizontalAlignment: Text.AlignHCenter
                visible: !!fan ? (fan.anchor <= minTemp || fan.anchor >= maxTemp) && fan.controlMode === Fancontrold.PwmFan.AnchorControl : false
                text: i18n("Fan anchor outside of graph.\nPlease set new anchor by clicking on the graph.")
            }

            Label {
                anchors.centerIn: parent

                visible: !daemonConnected

                text: "Fancontrold is not running."
                color: "red"
            }

            StatusPoint {
                id: currentPwm

                size: graph.fontSize
                visible: graphBackground.contains(center) && !!fan && fan.controlMode !== Fancontrold.PwmFan.NoControl
                fan: root.fan
            }
        }
    }

    FanControls {
        id: settingsArea

        fan: root.fan
        padding: root.margin
        visible: root.showControls && root.height >= height + 2*margin

        anchors {
            left: parent.left
            leftMargin: padding
            right: parent.right
            rightMargin: padding
            bottom: parent.bottom
            bottomMargin: padding
        }

        onTempDrawerRequested: root.tempDrawerRequested()
    }
}
