/*
 * Copyright (C) 2018  Malte Veerman <malte.veerman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


import QtQuick 2.15
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.3
import org.kde.kirigami 2.14 as Kirigami
import Fancontrold.Qml 1.0 as Fancontrold


ListView {
    id: listView

    required property QtObject fan

    implicitWidth: footerItem.implicitWidth
    clip: true
    reuseItems: true

    model: Fancontrold.Base.combinedTempModel

    section.property: "hwmon"
    section.delegate: Kirigami.ListSectionHeader {
        label: section
    }

    delegate: Kirigami.CheckableListItem {
        visible: type == Fancontrold.SensorProxyModel.Temp || type == Fancontrold.SensorProxyModel.VirtualTemp
        label: display
        subtitle: path
        reserveSpaceForIcon: false
        checked: fan.temps.includes(object) || fan.virtualTemps.includes(object)

        action: Kirigami.Action {
            onTriggered: {
                checked = !checked;

                if (type == Fancontrold.SensorProxyModel.Temp) {
                    var temps = fan.temps;

                    if (temps.includes(object)) {
                        var index = temps.indexOf(object);
                        temps.splice(index, 1);
                    } else {
                        temps.push(object);
                    }

                    fan.temps = temps;

                    checked = fan.temps.includes(object);
                } else if (type == Fancontrold.SensorProxyModel.VirtualTemp) {
                    var virtualTemps = fan.virtualTemps;

                    if (virtualTemps.includes(object)) {
                        var index = virtualTemps.indexOf(object);
                        virtualTemps.splice(index, 1);
                        checked = false;
                    } else {
                        virtualTemps.push(object);
                    }

                    fan.virtualTemps = virtualTemps;

                    checked = fan.virtualTemps.includes(object);
                }
            }
        }

        Button {
            visible: type == Fancontrold.SensorProxyModel.VirtualTemp

            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter

            icon.name: "edit-delete"

            onClicked: Fancontrold.Base.loader.removeVirtualTemp(Qt.resolvedUrl(path));
        }
    }

    footerPositioning: ListView.OverlayFooter

    footer: Rectangle {
        readonly property int mtf: mtfCombo.currentIndex

        implicitWidth: tempChooseLabel.contentWidth + mtfCombo.implicitWidth + 4 * Kirigami.Units.smallSpacing
        implicitHeight: childrenRect.height + 2 * Kirigami.Units.smallSpacing
        z: 3
        color: Kirigami.Theme.backgroundColor

        ColumnLayout {
            x: Kirigami.Units.smallSpacing
            y: Kirigami.Units.smallSpacing
            width: parent.width - 2 * Kirigami.Units.smallSpacing

            Button {
                Layout.alignment: Qt.AlignRight
                Layout.rightMargin: Kirigami.Units.smallSpacing

                text: i18n("Create virtual temp")

                onClicked: fileDialog.open()
            }

            RowLayout {
                Layout.fillWidth: true

                Label {
                    id: tempChooseLabel

                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: Kirigami.Units.smallSpacing

                    text: i18n("Temp value to choose:")
                }

                ComboBox {
                    id: mtfCombo

                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignRight
                    Layout.rightMargin: Kirigami.Units.smallSpacing

                    model: ["Lowest", "Highest", "Average"]
                    currentIndex: !!fan ? fan.mtf : 1

                    onCurrentIndexChanged: if (!!fan) { fan.mtf = currentIndex; }

                    Connections {
                        target: listView
                        function onFanChanged() { mtfCombo.currentIndex = !!fan ? fan.mtf : 1; }
                    }
                    Connections {
                        target: fan
                        function onMtfChanged() { mtfCombo.currentIndex = fan.mtf; }
                    }
                }
            }
        }
    }

    FileDialog {
        id: fileDialog

        title: i18n("Please choose a virtual temp file")
        folder: shortcuts.home
        visible: false

        onAccepted: {
            Fancontrold.Base.loader.addVirtualTemp(fileUrl);
        }
        onRejected: {
            close();
        }
    }
}
