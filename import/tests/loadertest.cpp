/*
 * Copyright 2015  <copyright holder> <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "loadertest.h"

#include <QtTest/QtTest>
#include <QtTest/QSignalSpy>


void LoaderTest::initTestCase()
{
    m_loader = new TestLoader;

    m_rpms << (QList<QString *>() << new QString << new QString << new QString);
    m_rpms << (QList<QString *>() << new QString << new QString << new QString);

    m_temps << (QList<QString *>() << new QString << new QString << new QString);
    m_temps << (QList<QString *>() << new QString << new QString << new QString);

    m_pwms << (QList<QString *>() << new QString << new QString);
    m_pwms << (QList<QString *>() << new QString << new QString);

    m_pwmModes << (QList<QString *>() << new QString << new QString);
    m_pwmModes << (QList<QString *>() << new QString << new QString);

    QCOMPARE(m_loader->hwmons().size(), 0);

    auto hwmon0 = new TestHwmon(QStringLiteral("radeon"), m_rpms.at(0), m_pwms.at(0), m_temps.at(0), 0, m_loader);
    QCOMPARE(hwmon0->pwmFans().size(), 2);

    auto hwmon1 = new TestHwmon(QStringLiteral("coretemp"), m_rpms.at(1), m_pwms.at(1), m_temps.at(1), 1, m_loader);
    QCOMPARE(hwmon0->pwmFans().size(), 2);

    m_loader->addHwmon(hwmon0);
    m_loader->addHwmon(hwmon1);

    QCOMPARE(m_loader->hwmons().size(), 2);
}

void LoaderTest::cleanupTestCase()
{
    delete m_loader;
}

void LoaderTest::init()
{
    // Called before each testfunction is executed
}

void LoaderTest::cleanup()
{
    // Called after every testfunction
}

void LoaderTest::createConfigTest()
{
    auto pwmFan = m_loader->pwmFan(0, 1);
    QVERIFY(pwmFan);

    auto temp = m_loader->temp(1, 1);
    QVERIFY(temp);

    pwmFan->setTemps(QList<Temp*>{ temp });
    pwmFan->setCurve(Curve() << QPoint(20, 100) << QPoint(60, 200));
    pwmFan->setMinStart(120);
    pwmFan->setMinPwm(80);
    pwmFan->setControlMode(PwmFan::ControlMode::CurveControl);

    pwmFan = m_loader->pwmFan(1, 2);
    QVERIFY(pwmFan);

    temp = m_loader->temp(1, 3);
    QVERIFY(temp);

    pwmFan->setTemps(QList<Temp*>{ temp });
    pwmFan->setCurve(Curve() << QPoint(30, 120) << QPoint(70, 255));
    pwmFan->setMinStart(110);
    pwmFan->setMinPwm(75);
    pwmFan->setControlMode(PwmFan::ControlMode::CurveControl);

    auto config = m_loader->createConfig();
    auto c = config.object();

    auto channels = c.value(QStringLiteral("channels"));
    QVERIFY(channels.isArray());

    const auto channelsArray = channels.toArray();
    QCOMPARE(channelsArray.size(), 2);

    for (const auto &channelConfig : channelsArray)
    {
        QVERIFY(channelConfig.isObject());

        auto channelObject = channelConfig.toObject();

        auto hwmonDevicePathValue = channelObject.value(QStringLiteral("hwmon_device_path"));
        QVERIFY(hwmonDevicePathValue.isString());

        auto hwmonDevicePath = hwmonDevicePathValue.toString();

        auto hwmon = m_loader->hwmon(hwmonDevicePath);
        QVERIFY(hwmon);

        auto indexValue = channelObject.value(QStringLiteral("index"));
        QVERIFY(indexValue.isDouble());

        int index = indexValue.toDouble();

        auto pwm = hwmon->pwmFans().value(index);
        QVERIFY(pwm);

        auto tempsValue = channelObject.value(QStringLiteral("temps"));
        QVERIFY(tempsValue.isArray());

        auto tempsArray = tempsValue.toArray();
        QCOMPARE(tempsArray.size(), 1);

        auto controlValue = channelObject.value(QStringLiteral("control"));
        QVERIFY(controlValue.isObject());

        auto controlObject = controlValue.toObject();
        auto curveValue = controlObject.value(QStringLiteral("Curve"));

        QVERIFY(curveValue.isObject());

        auto curveObject = curveValue.toObject();
        Curve curve;
        foreach (const auto &key, curveObject.keys())
        {
            int x = key.toInt();
            int y = curveObject.value(key).toDouble();
            curve << QPoint(x, y);
        }

        auto minStartValue = channelObject.value(QStringLiteral("min_start"));
        QVERIFY(minStartValue.isDouble());
        //QCOMPARE(minStartValue.toDouble(), 120);

        auto minPwmValue = channelObject.value(QStringLiteral("min_pwm"));
        QVERIFY(minPwmValue.isDouble());
    }
}

QTEST_MAIN(LoaderTest);
